import React, { useEffect, MouseEvent } from "react";
import "./ImageModal.scss";

import { useModalContext } from "../../contexts/ModalContext";
import { ModalContextValue } from "../../ts/types/contextvalue_types";

import CloseButton from "../../ui/CloseButton";

const ImageModal: React.FC = () => {

  const modalContext: ModalContextValue = useModalContext();

  const handleClose: () => void = () => modalContext.setShowImageModal(false);

  const handleModalBackgroundClick: (_e: MouseEvent<HTMLDivElement>) => void = (_e) => modalContext.reset();

  const cancelPropagation: (e: MouseEvent<HTMLDivElement>) => void = (e) => e.stopPropagation();

  useEffect(() => {
    /* IMPORTANT: 
     * In case of using this with React make sure you don't accidentally use import { KeyboardEvent } from "react";.
     * This leads to the not assignable to type 'EventListener' exception. (Credit: stackoverflow manu) 
     */
    const modalKeyHandler = (e: KeyboardEvent) => { if (e.key === "Escape") return modalContext.reset() };

    window.addEventListener("keydown", modalKeyHandler);

    return () => window.removeEventListener("keydown", modalKeyHandler);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return(
    <div className="imagemodal" onClick={ handleModalBackgroundClick }>
      <div className="imagemodal__window" onClick={ cancelPropagation }>
	<CloseButton onClick={ handleClose } />
	<img className="imagemodal__window__img" src={ modalContext.imageModalSrc } />
      </div>
    </div>
  );
};

export default ImageModal;
