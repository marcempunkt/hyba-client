import React, { ReactNode } from "react";

interface Props {
  children: ReactNode;
}

const ModalContent: React.FC<Props> = (props: Props) => {
  return(<div className="modal__content"> { props.children } </div>);
};

export default ModalContent;
