import React, { useState, useEffect } from "react";
import axios from "axios";

import { useFriendsContext } from "../../contexts/FriendsContext";
import { useUserContext } from "../../contexts/UserContext";
import { useModalContext } from "../../contexts/ModalContext";
import { FriendsContextValue, UserContextValue, ModalContextValue } from "../../ts/types/contextvalue_types";

import Modal from "./Modal";
import ModalTitle from "./ModalTitle";
import ModalButtonSection from "./ModalButtonSection";
import ModalButton from "./ModalButton";

const RemoveFriendModal: React.FC = () => {

    const friendsContext: FriendsContextValue = useFriendsContext();
    const userContext: UserContextValue = useUserContext();
    const modalContext: ModalContextValue = useModalContext();

    const [username, setUsername] = useState<string>("")

    const handleCancelClick = () => modalContext.setShowRemoveFriendModal(false);

    const handleRemoveClick = () => {
        friendsContext.removeFriend(friendsContext.removeFriendId);
        modalContext.setShowRemoveFriendModal(false);
    };

    useEffect(() => {
        /* declaring CancelToken to cancel the fetch get call in useEffect cleanup */
        const CancelToken = axios.CancelToken;
        const source = CancelToken.source();

        /* Getting the username */
        userContext.getUsernameById(friendsContext.removeFriendId, source.token).then(username => {
            setUsername(username);
        });

        return () => source.cancel()
    }, []);

    return(
        <Modal>
	  <ModalTitle>Permanently remove { username }?</ModalTitle>

	  <ModalButtonSection>
	    <ModalButton label="No" onClick={ handleCancelClick } />
	    <ModalButton label="Remove" className="btn--primary" onClick={ handleRemoveClick } />
	  </ModalButtonSection>
        </Modal>
    );
}

export default RemoveFriendModal;
