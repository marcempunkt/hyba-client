import React, { useState, useEffect, FormEvent, ChangeEvent } from "react";
import axios from "axios";
import moment, { Moment } from "moment-timezone";
import "./ShowProfileModal.scss";

import { ReactComponent as Send } from "../../assets/feathericons/send.svg";

import { useAppContext } from "../../contexts/AppContext";
import { useMessageContext } from "../../contexts/MessageContext";
import { useUserContext } from "../../contexts/UserContext";
import { useModalContext } from "../../contexts/ModalContext";
import { useFriendsContext } from "../../contexts/FriendsContext";
import { AppContextValue,
	 UserContextValue,
	 MessageContextValue,
	 ModalContextValue,
	 FriendsContextValue } from "../../ts/types/contextvalue_types";
import { SendMessage } from "../../ts/types/message_types";
import { Friend } from "../../ts/types/friends_types";

import DefaultImage from "../../assets/images/avatar.png";
import Modal from "./Modal";

const ShowProfileModal: React.FC = () => {

  const appContext: AppContextValue = useAppContext();
  const userContext: UserContextValue = useUserContext();
  const messageContext: MessageContextValue = useMessageContext();
  const modalContext: ModalContextValue = useModalContext();
  const friendsContext: FriendsContextValue = useFriendsContext();

  const [message, setMessage] = useState<string>("");
  const [registerDate, setRegisterDate] = useState<Date>();
  const [friend, setFriend] = useState<Friend | undefined>(undefined);

  useEffect(() => {
    /**
     * @effect
     */
    const currentFriend: Friend | undefined = friendsContext.getFriend(modalContext.showProfileId);
    setFriend(currentFriend);

    /* get the register_date of this user */
    if (currentFriend) {
      const CancelToken = axios.CancelToken;
      const source = CancelToken.source();

      /* get register date of a user */
      userContext.getRegisterDateById(modalContext.showProfileId, source.token)
		   .then((date: number) => {
		       if (typeof date !== "object") setRegisterDate(new Date(date));
		       /* setRegisterDate(new Date(date)); */
		   })
		   .catch((_err: unknown) => {
		     setRegisterDate(undefined);
		   });

      return () => source.cancel()
    }

  }, [modalContext.showProfileId, modalContext.showProfileModal]);
 
  const handleChange = (e: ChangeEvent<HTMLInputElement>) => setMessage(e.currentTarget.value);
  const handleCloseClick = () => modalContext.setShowProfileModal(false);

  const handleUsername: () => string = () => {
    if (!friend) return "unknown";
    return friend.friendName;
  };

  const handlePicSrc: () => string = () => {
    if (!friend) return DefaultImage;
    return `${appContext.API_URL}/users/get/avatar/${ modalContext.showProfileId }`;
  };

  const handleDate: () => string = () => {
    /**
     * return the correctly formatted string of the registerDate of a user
     * @private
     */
    if (!registerDate) return "unknown";
    const format: string = "DD. MMMM YYYY";
    const registerDateMoment: Moment = moment(registerDate);
    return(registerDateMoment.tz(appContext.timezone).format(format));
  };

  const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    appContext.setShowFriends(false);
    appContext.setShowDashboard(false);
    appContext.setShowTimeline(false);
    modalContext.setShowProfileModal(false);

    if (message) {
      const msg: SendMessage = {
	from_user: userContext.loggedInUserId,
	to_user: modalContext.showProfileId,
	content: message,
      };

      messageContext.sendMessage(msg);
      setMessage("");
    }

    messageContext.setShowMessagesFromUserId(modalContext.showProfileId);
  };

  return(
    <Modal>
      <div className="modal__window__showprofile">

        <div className="showprofile__topbar">

          <img
	    className="showprofile__topbar__avatar"
	    src={ handlePicSrc() }
	    alt="avatar" />

	  <div className="showprofile__topbar__userinfo">
            <span className="showprofile__topbar__username">{ handleUsername() }</span>
            <span className="showprofile__topbar__registerdate">
	      Member since: { handleDate() }
	    </span>
	  </div>

        </div>

	{
	  /* if they are friends render send message form if not then don't */
	  (friend) ?
	  <form className="showprofile__form" onSubmit={ handleSubmit }>
	    <input 
	      type="text"
	      className="showprofile__form__sendfield"
	      placeholder={ `Message @${ handleUsername() }` }
	      onChange={ handleChange }
	      value={ message } />

            <button className="showprofile__form__submit-btn" type="submit"><Send /></button>
	  </form>
	  :
	  null
	}

	<div className="modal__window__btns">
          <button className="btn margin-top full-width" onClick={ handleCloseClick }>Close</button>
	</div>

      </div>
    </Modal>
  );
}

export default ShowProfileModal;
