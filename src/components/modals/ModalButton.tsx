import React from "react";

interface Props {
  className?: string;
  label: string;
  onClick: () => void;
}

const ModalButton: React.FC<Props> = (props: Props) => {
  return(
    <button
      className={ "btn full-width margin-top " + ((props.className) ? props.className : "") }
      onClick={ props.onClick }>
      { props.label }
    </button>
  );
};

export default ModalButton;
