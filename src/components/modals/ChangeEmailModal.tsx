import React, { useState, ChangeEvent, FormEvent } from "react";
import axios, { AxiosResponse, AxiosError } from "axios";
import logger from "../../utils/logger";

import { useAppContext } from "../../contexts/AppContext";
import { useUserContext } from "../../contexts/UserContext";
import { useNotificationContext } from "../../contexts/NotificationContext";
import { useModalContext } from "../../contexts/ModalContext";
import { AppContextValue,
	 UserContextValue,
	 NotificationContextValue,
	 ModalContextValue } from "../../ts/types/contextvalue_types";
import { NotificationType } from "../../ts/types/notification_types";
import { Response } from "../../ts/types/axios_types";

import Modal from "./Modal";
import Form from "../form/Form";
import Input from "../form/Input";
import SubmitButton from "../form/SubmitButton";
import CloseButton from "../../ui/CloseButton";

const ChangeEmailModal: React.FC = () => {

    const appContext: AppContextValue = useAppContext();
    const userContext: UserContextValue = useUserContext();
    const notifContext: NotificationContextValue = useNotificationContext();
    const modalContext: ModalContextValue = useModalContext();

    const [oldEmail, setOldEmail] = useState<string>("");
    const [newEmail, setNewEmail] = useState<string>("");
    const [password, setPassword] = useState<string>("");
    const [error, setError] = useState<string>("");

    const handleOldEmailChange = (e: ChangeEvent<HTMLInputElement>) => setOldEmail(e.currentTarget.value);
    const handleNewEmailChange = (e: ChangeEvent<HTMLInputElement>) => setNewEmail(e.currentTarget.value);
    const handlePasswordChange = (e: ChangeEvent<HTMLInputElement>) => setPassword(e.currentTarget.value);

    const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        if (!userContext.token) return logger.error.blue("ChangeEmailModal", "No Authentication Token found.");
        // check if form is filled
        if (!oldEmail || !newEmail || !password) {
            return setError("Please fill out the form");
        }

        axios.patch(
            `${appContext.API_URL}/users/change/email`,
            { oldEmail, newEmail, password },
            { headers: { Authorization: `Bearer: ${userContext.token}` } }
        ).then((res: AxiosResponse<Response>) => {
	    notifContext.create(NotificationType.SUCCESS, res.data.message);
	    modalContext.setShowChangeEmailModal(false);
	}).catch((err: AxiosError<Response>) => {
	    if (err.response) {
	        setError(err.response.data.message);
	    }
	});
    };

    const handleCancelClick = () => modalContext.setShowChangeEmailModal(false);

    return(
        <Modal windowWidth="500px">
	    <CloseButton  onClick={ handleCancelClick } />
	    <Form
	        title="Change my E-mail"
	        onSubmit={ handleSubmit }
	        error={ error } >

	        <Input
	            htmlId="oldEmail"
	            label="Current E-mail"
	            inputType="email"
	            value={ oldEmail }
	            onChange={ handleOldEmailChange } />

	        <Input
	            htmlId="newEmail"
	            label="New E-mail"
	            inputType="email"
	            value={ newEmail }
	            onChange={ handleNewEmailChange } />

	        <Input
	            htmlId="password"
	            label="Password"
	            inputType="password"
	            value={ password }
	            onChange={ handlePasswordChange } />

	        <SubmitButton
	            className="btn--primary"
	            isDisabled={ (oldEmail && newEmail && password) ? false : true }
	            innerHtml="Change E-mail" />
	    </Form>
        </Modal>
    );
};

export default ChangeEmailModal;
