import React, { useEffect } from "react";
import { useNavigate } from "react-router-dom";

import { useAppContext } from "../../contexts/AppContext";
import { useSocketContext } from "../../contexts/SocketContext";
import { useUserContext } from "../../contexts/UserContext";
import { useMessageContext } from "../../contexts/MessageContext";
import { useFriendsContext } from "../../contexts/FriendsContext";
import { usePostContext } from "../../contexts/PostContext";
import { useBlogContext } from "../../contexts/BlogContext";
import { useModalContext } from "../../contexts/ModalContext";
import { useCallContext } from "../../contexts/CallContext";
import { AppContextValue,
	 SocketContextValue,
	 UserContextValue,
	 MessageContextValue,
	 FriendsContextValue,
	 PostContextValue,
	 BlogContextValue,
	 ModalContextValue,
	 CallContextValue } from "../../ts/types/contextvalue_types";
import { TinyInt } from "../../ts/types/mariadb_types";

import Modal from "./Modal";
import ModalTitle from "./ModalTitle";
import ModalButtonSection from "./ModalButtonSection";
import ModalButton from "./ModalButton";

const LogoutModal: React.FC = () => {

  const appContext: AppContextValue = useAppContext();
  const socketContext: SocketContextValue = useSocketContext();
  const userContext: UserContextValue = useUserContext();
  const messageContext: MessageContextValue = useMessageContext();
  const friendsContext: FriendsContextValue = useFriendsContext();
  const postContext: PostContextValue = usePostContext();
  const blogContext: BlogContextValue = useBlogContext();
  const modalContext: ModalContextValue = useModalContext();
  const callContext: CallContextValue = useCallContext();

  const navigate = useNavigate();

  const handleCancelClick = () => modalContext.setShowLogoutModal(false);

  const handleLogout = () => {
    appContext.reset();
    socketContext.reset();
    userContext.reset();
    messageContext.reset();
    friendsContext.reset();
    postContext.reset();
    blogContext.reset();
    modalContext.reset();
    callContext.reset();
    localStorage.removeItem("auth");
    navigate("/getting-started");
  };

  /* KeyListener */
  useEffect(() => {
    const windowKeyHandler = (e: KeyboardEvent) => {
      if (e.key === "Enter") return handleLogout();
    };

    window.addEventListener("keydown", windowKeyHandler);

    return () => window.removeEventListener("keydown", windowKeyHandler);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Modal>
      <ModalTitle>Are you sure?</ModalTitle>

      <ModalButtonSection>
	<ModalButton label="No" onClick={ handleCancelClick } />
	<ModalButton label="Log out!" className="btn--primary" onClick={ handleLogout } />
      </ModalButtonSection>
    </Modal>
  );
};

export default LogoutModal;
