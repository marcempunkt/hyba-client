import React, { useState, useEffect } from "react";
import axios from "axios";

import { useUserContext } from "../../contexts/UserContext";
import { useModalContext } from "../../contexts/ModalContext";
import { useFriendsContext } from "../../contexts/FriendsContext";
import { UserContextValue, ModalContextValue, FriendsContextValue } from "../../ts/types/contextvalue_types";

import Modal from "./Modal";
import ModalTitle from "./ModalTitle";
import ModalButtonSection from "./ModalButtonSection";
import ModalButton from "./ModalButton";

const UnblockUserModal: React.FC = () => {

  const modalContext: ModalContextValue = useModalContext();
  const userContext: UserContextValue = useUserContext();
  const friendsContext: FriendsContextValue = useFriendsContext();

  const [username, setUsername] = useState<string>("");

  const handleCancelClick = () => modalContext.setShowUnblockUserModal(false);

  const handleUnblock = () => {
    friendsContext.unblockFriend(modalContext.blockOrUnblockUserId);
    modalContext.setShowUnblockUserModal(false);
  };

  /* KeyListener */
  useEffect(() => {
    const windowKeyHandler = (e: KeyboardEvent) => {
      if (e.key === "Enter") return handleUnblock();
    };

    window.addEventListener("keydown", windowKeyHandler);

    return () => window.removeEventListener("keydown", windowKeyHandler);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    const CancelToken = axios.CancelToken;
    const source = CancelToken.source();

    /* get username by showProfileId */
    userContext.getUsernameById(modalContext.blockOrUnblockUserId, source.token).then(username => {
      setUsername(username);
    }).catch((_err) => {});

    return () => source.cancel()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [modalContext.blockOrUnblockUserId]);



  return (
    <Modal>
      <ModalTitle>Unblock { username }?</ModalTitle>

      <ModalButtonSection>
	<ModalButton label="No" onClick={ handleCancelClick } />
	<ModalButton label="Unblock!" className="btn--primary" onClick={ handleUnblock } />
      </ModalButtonSection>
    </Modal>
  );
};

export default UnblockUserModal;
