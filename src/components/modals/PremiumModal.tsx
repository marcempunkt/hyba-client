import React from "react";
import "./PremiumModal.scss";

import { useModalContext } from "../../contexts/ModalContext";
import { ModalContextValue } from "../../ts/types/contextvalue_types";

import Modal from "./Modal";
import ModalTitle from "./ModalTitle";
import ModalContent from "./ModalContent";
import CloseButton from "../../ui/CloseButton";

const PremiumModal: React.FC = () => {

  const modalContext: ModalContextValue = useModalContext();

  const handleClose: () => void = () => {
    modalContext.setShowPremiumModal(false);
  };

  return(
    <Modal windowWidth="95vw" windowHeight="95vh">
      <CloseButton onClick={ handleClose } />
      <ModalTitle>How to become a Babbler?</ModalTitle>
      <ModalContent>
	<div className="selectable premiummodal">
	  <p>How to contribute?</p>
	  <p>financial support</p>
	  <p>Donate me coffe</p>
	</div>
      </ModalContent>
    </Modal>
  );
};

export default PremiumModal;

