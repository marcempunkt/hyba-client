import React from "react";

import { useFriendrequestsContext } from "../../contexts/FriendrequestsContext";
import { FriendrequestsContextValue } from "../../ts/types/contextvalue_types";
import { Friendrequest } from "../../ts/types/friends_types";

import PendingItem from "./PendingItem";

const Pendings: React.FC = () => {

  const friendrequestsContext: FriendrequestsContextValue = useFriendrequestsContext();

  return(
    <ul className="userlist">
      { friendrequestsContext.pendings.map((pending: Friendrequest) => (
        <PendingItem
	  key={ pending.id }
          pendingId={ pending.id }
          fromUser={ pending.from_user }
          toUser={ pending.to_user }
        />
      )) }
    </ul>
  );
}

export default Pendings;
