import React, { ReactNode } from "react";

interface Props {
  icon: ReactNode;
  label: string;
  onClick?: () => void;
  actions?: Array<JSX.Element>;
  className?: string;
}

const QuickmenuItem: React.FC<Props> = (props: Props) => {

  return(
    <li className={ "quickmenu__item " + ((props.className) ? props.className : "")  } onClick={ props.onClick }>

      { props.icon }

      <span className="quickmenu__item__label">{ props.label }</span>

      { (props.actions) ?
	<div className="quickmenu__item__actions">
	  { props.actions.map((action: JSX.Element) => action) }
	</div>
	: null}

    </li>
  );
};

export default QuickmenuItem;
