import React, { MouseEvent, useEffect, useState } from "react";
import "./DirectMessagesUserlistItem.scss";

import { useSocketContext } from "../../contexts/SocketContext";
import { useFriendsContext } from "../../contexts/FriendsContext";
import { useContextmenuContext } from "../../contexts/ContextmenuContext";
import { useMessageContext } from "../../contexts/MessageContext";
import { useModalContext } from "../../contexts/ModalContext";
import { useUserContext } from "../../contexts/UserContext";
import { useAppContext } from "../../contexts/AppContext";
import { SocketContextValue,
	 FriendsContextValue,
	 ContextmenuContextValue,
	 MessageContextValue,
	 ModalContextValue,
	 UserContextValue,
	 AppContextValue } from "../../ts/types/contextvalue_types";
import { FriendWithAvatar } from "../../ts/types/friends_types";
import { TinyInt } from "../../ts/types/mariadb_types";

import { ReactComponent as X } from "../../assets/feathericons/x.svg";
import { ReactComponent as User } from "../../assets/feathericons/user.svg";
import { ReactComponent as UserMinus } from "../../assets/feathericons/user-minus.svg";
import { ReactComponent as UserX } from "../../assets/feathericons/user-x.svg";
import { ReactComponent as Trash } from "../../assets/feathericons/trash-2.svg";
import ContextmenuItem from "../contextmenu/ContextmenuItem";

interface Props {
    userId: number;
}

const DirectMessagesUserlistItem: React.FC<Props> = (props: Props) => {

    const socketContext: SocketContextValue = useSocketContext();
    const friendsContext: FriendsContextValue = useFriendsContext();
    const contextmenuContext: ContextmenuContextValue = useContextmenuContext();
    const messageContext: MessageContextValue = useMessageContext();
    const modalContext: ModalContextValue = useModalContext();
    const userContext: UserContextValue = useUserContext();
    const appContext: AppContextValue = useAppContext();

    const [friend, setFriend] = useState<FriendWithAvatar | undefined>(undefined);

    useEffect(() => {
        /**
         * Get the correct Friend Object according to the passed down userId
         * @effect
         */
        const currentFriend: FriendWithAvatar | undefined = friendsContext.getFriend(props.userId);
        if (currentFriend) {
            setFriend(currentFriend);
        } else {
            setFriend(undefined);
        }
    }, [props.userId, friendsContext.friends]);

    const handleName: () => string = () => {
        /**
         * shorten the username if too long
         * @private
         */
        if (!friend) return "unknown";

        if (friend.friendName.length > 10) {
            return (friend.friendName.slice(0, 9) + "...");
        }

        return friend.friendName;
    };

    const getExtraClasses: () => string = () => {
        // className={ ((props.isActive) ? "useritem--active" : "") }
        // className={ "useritem__icon" + getProfilePicClassNames() }
        if (messageContext.showMessagesFromUserId === props.userId) return " directmessagesuserlistitem--active";
        return "";
    };

    const getAvatarExtraClasses: () => string = () => {
        /**
         * returns additional className for the user picture 
         * depending if blocked
         * @private
         * 
         * returns  {string}  classes
         */
        let classes: string = "";

        if (!friend) return classes += " status--offline";

        classes += userContext.handleStatusClasses(friend.status);

        /* Check if blocked */
        if (friend.blocked === TinyInt.TRUE) {
            classes += " useritem__icon--blocked";
        }

        return classes;
    };

    const handleClick = () => {
        messageContext.setShowMessagesFromUserId(props.userId);
        appContext.setShowFriends(false);
        appContext.setShowDashboard(false);
        appContext.setShowTimeline(false);
    };

    const handleBlock = () => {
        modalContext.setBlockOrUnblockUserId(props.userId);
        modalContext.setShowBlockUserModal(true);
    };

    const handleUnblock = () => {
        modalContext.setBlockOrUnblockUserId(props.userId);
        modalContext.setShowUnblockUserModal(true);
    };

    const handleDeleteChat = (e: MouseEvent<SVGElement> | undefined = undefined) => {
        if (e) { e.stopPropagation(); }
        modalContext.setShowDeleteChatModal(true);
        modalContext.setDeleteChatFromUserId(props.userId);
    };

    const showProfileMenuItem = <ContextmenuItem
                                    key={ 1 }
                                    icon={ <User /> }
                                    description="Show Profile"
                                    handleClick={ () => contextmenuContext.handleShowProfile(props.userId) }/>;

    const removeFriendMenuItem = <ContextmenuItem
                                     key={ 2 }
                                     icon={ <UserMinus /> }
                                     description="Remove Friend"
				     handleClick={ () => contextmenuContext.handleRemoveFriend(props.userId) }/>;

    const blockFriendMenuItem = <ContextmenuItem
                                    key={ 3 }
                                    icon={ <UserX /> }
                                    description="Block"
                                    handleClick={ handleBlock }/>;

    const unblockFriendMenuItem = <ContextmenuItem
                                      key={ 4 }
                                      icon={ <UserX /> }
                                      description="Unblock"
                                      handleClick={ handleUnblock }/>;

    const deleteChatMenuItem = <ContextmenuItem
                                   key={ 5 }
                                   icon={ <Trash /> }
                                   description="Delete Chat"
                                   handleClick={ handleDeleteChat } />;

    const handleContextmenu = (e: MouseEvent<HTMLLIElement>) => {
        e.preventDefault();

        /* Spawn Contextmenu */
        contextmenuContext.create(e.pageX, e.pageY);

        /* aren't friends */
        if (!friend) return contextmenuContext.setComponents([deleteChatMenuItem]);

        /* is blocked */
        if (friend.blocked) return contextmenuContext.setComponents([
            showProfileMenuItem,
            deleteChatMenuItem,
            removeFriendMenuItem,
            unblockFriendMenuItem
        ]);

        /* default => are friends & isn't blocked */
        return contextmenuContext.setComponents([
            showProfileMenuItem,
            deleteChatMenuItem,
            removeFriendMenuItem,
            blockFriendMenuItem
        ]);
    };

    return(
        <li
            className={"directmessagesuserlistitem" + getExtraClasses() }
            onClick={ handleClick }
            onContextMenu={ handleContextmenu } >

            <img
                className={ "directmessagesuserlistitem__avatar" + getAvatarExtraClasses() }
                alt={ `${props.userId}` }
                src={ friendsContext.getAvatar(friend) } />

            <span className="directmessagesuserlistitem__name" >{ handleName() }</span>

            <X key={ 1 } className="directmessagesuserlistitem__delete" onClick={ handleDeleteChat } />
        </li>
    );
};

export default DirectMessagesUserlistItem;

