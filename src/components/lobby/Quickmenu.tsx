import React, { MouseEvent } from "react";
import "./Quickmenu.scss";

import { ReactComponent as Users } from "../../assets/feathericons/users.svg";
import { ReactComponent as Feather } from "../../assets/feathericons/feather.svg";
import { ReactComponent as Layers } from "../../assets/feathericons/layers.svg";
import { ReactComponent as Plus } from "../../assets/feathericons/plus.svg";

import { useAppContext } from "../../contexts/AppContext";
import { useFriendsContext } from "../../contexts/FriendsContext";
import { useMessageContext } from "../../contexts/MessageContext";
import { usePostContext } from "../../contexts/PostContext";
import { AppContextValue, FriendsContextValue, MessageContextValue, PostContextValue } from "../../ts/types/contextvalue_types";

import QuickmenuItem from "./QuickmenuItem";
import Tooltip from "../Tooltip";

const Quickmenu: React.FC = () => {
  /**
   * Quickmenu Section inside the Lobby Component
   * @component
   */
  const appContext: AppContextValue = useAppContext();
  const friendsContext: FriendsContextValue = useFriendsContext();
  const messageContext: MessageContextValue = useMessageContext();
  const postContext: PostContextValue = usePostContext();

  const clickHook: () => void = () => {
    /**
     * hook function that should be called everytime a quickmenuitem is clicked
     * handles the state
     * @private
     */
    /* set all appcontext to false */
    appContext.setShowSettings(false);
    appContext.setShowFriends(false);
    appContext.setShowDashboard(false);
    appContext.setShowTimeline(false);
    /* set all friendscontext to false */
    friendsContext.setShowOnline(false);
    friendsContext.setShowAll(false);
    friendsContext.setShowPending(false);
    friendsContext.setShowBlocked(false);
    friendsContext.setShowAddFriend(false);
    /* reset showMessageFromUser */
    messageContext.setShowMessagesFromUserId(0);
  };

  const handleFriendsClick = () => {
    clickHook();
    appContext.setShowFriends(true);
    friendsContext.setShowOnline(true);
  };

  const handleFriendsPlusClick = (e: MouseEvent<SVGElement>) => {
    e.stopPropagation();
    clickHook();
    appContext.setShowFriends(true);
    friendsContext.setShowAddFriend(true);
  };

  const handleTimelineClick = () => {
    clickHook();
    appContext.setShowTimeline(true);
    if (!postContext.newPostContent && !postContext.newPostImages.length) {
      postContext.setShowCreatePost(false);
    }
  };

  const handleTimelinePlusClick = (e: MouseEvent<SVGElement>) => {
    e.stopPropagation();
    clickHook();
    appContext.setShowTimeline(true);
    postContext.setShowCreatePost(true);
  };

  const handleDashboardClick = () => {
    clickHook();
    appContext.setShowDashboard(true);
  };

  return(
    <ul className="quickmenu">

      <QuickmenuItem
	icon={ <Layers className="quickmenu__item__icon" /> }
	label="Dashboard"
        className={ (appContext.showDashboard) ? "quickmenu__item--active" : "" }
	onClick={ handleDashboardClick } />

      <QuickmenuItem
        icon={ <Users className="quickmenu__item__icon" /> }
        label="Friends"
        className={ (appContext.showFriends) ? "quickmenu__item--active" : "" }
        onClick={ handleFriendsClick }
	actions={ [
	  <Tooltip key={ 0 } title="Add&nbsp;friend"><Plus className="quickmenu__item__actions__add" onClick={ handleFriendsPlusClick } /></Tooltip>
	] } />

      <QuickmenuItem
        icon={ <Feather className="quickmenu__item__icon" /> }
        label="Timeline"
        className={ (appContext.showTimeline) ? "quickmenu__item--active" : "" }
        onClick={ handleTimelineClick }
	actions={ [
	  <Tooltip key={ 1 } title="New&nbsp;Post"><Plus className="quickmenu__item__actions__add" onClick={ handleTimelinePlusClick } /></Tooltip>
	] } />

    </ul>
  )
};

export default Quickmenu;
