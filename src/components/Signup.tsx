import React, { useState, FormEvent } from "react";
import axios, { AxiosRequestConfig, AxiosResponse, AxiosError } from "axios";
import "./Signup.scss";

import { useNotificationContext } from "../contexts/NotificationContext";
import { useAppContext } from "../contexts/AppContext";
import { NotificationContextValue, AppContextValue } from "../ts/types/contextvalue_types";
import { NotificationType } from "../ts/types/notification_types";
import { Response } from "../ts/types/axios_types";

import Input from "./form/Input";
import Form from "./form/Form";
import SubmitButton from "./form/SubmitButton";

interface Props {
    onChangeView: () => void;
}

const Signup: React.FC<Props> = (props: Props) => {
    /**
     * Component for rendering a Signup/Register Form
     * @component
     */
    const notifContext: NotificationContextValue = useNotificationContext();
    const appContext: AppContextValue = useAppContext();

    const [email, setEmail] = useState<string>("");
    const [username, setUsername] = useState<string>("");
    const [password, setPassword] = useState<string>("");
    const [checkPassword, setCheckPassword] = useState<string>("");
    const [disabled, setDisabled] = useState<boolean>(false);

    const [error, setError] = useState<string>("");

    const handleEmailChange = (e: FormEvent<HTMLInputElement>) => (setEmail(e.currentTarget.value));
    const handleUsernameChange = (e: FormEvent<HTMLInputElement>) => (setUsername(e.currentTarget.value));
    const handlePasswordChange = (e: FormEvent<HTMLInputElement>) => (setPassword(e.currentTarget.value));
    const handleSecondPwdChange = (e: FormEvent<HTMLInputElement>) => (setCheckPassword(e.currentTarget.value));

    const clearAllState = () => {
        setEmail("");
        setUsername("");
        setPassword("");
        setCheckPassword("");
        setError("");
    };

    const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
        /**
         * Handles Signup/Register of a new user
         * @private
         *
         * @param  {FormEvent<HTMLFormElement>}  e
         */
        e.preventDefault();
        if (!email && !username && !password && !checkPassword) return setError("State is incomplete. This shouldn't happen at all!");
        /* if it is not the same */
        if (!(password === checkPassword)) return setError("Passwords don't match");

        setDisabled(true);

        axios.post(
            `${appContext.API_URL}/users/register`,
            { email, username, password },
        ).then((res: AxiosResponse<Response>) => {
            if (res.data.message) {
                notifContext.create(NotificationType.SUCCESS, res.data.message);
                clearAllState();
                props.onChangeView();
            }
        }).catch((err: AxiosError<Response>) => {
	    setDisabled(false);
            if (err.response) {
                setError(err.response.data.message);
            } else {
                setError("Could not connect to Server. Please try again");
            }
        });
    };

    return(
        <div className="start__form">
            <Form
	        title="Create a new account"
	        onSubmit={ handleSubmit }
	        error={ error } >

	        <Input
	            htmlId="email"
	            label="E-Mail"
	            inputType="email"
	            value={ email }
	            onChange={ handleEmailChange } />

	        <Input
	            htmlId="username"
	            label="Username"
	            inputType="text"
	            value={ username }
	            onChange={ handleUsernameChange } />

	        <Input
	            htmlId="password"
	            label="Password"
	            inputType="password"
	            value={ password }
	            onChange={ handlePasswordChange } />

	        <Input
	            htmlId="check-password"
	            label="Retype your password"
	            inputType="password"
	            value={ checkPassword }
	            onChange={ handleSecondPwdChange } />

	        <span className="start__form__changeview" onClick={ props.onChangeView }>{ "I already have an account." }</span>

	        <SubmitButton
	            className="btn--primary full-width margin-top"
	            isDisabled={ disabled || ((email && password) ? false : true) }
	            innerHtml="Sign me up Scottie!" />
            </Form>
        </div>
    );
};

export default Signup;
