import React, { useState, useEffect } from "react";
import "./ChatContainer.scss";

import { useMessageContext } from "../../contexts/MessageContext";
import { useFriendsContext } from "../../contexts/FriendsContext";
import { useCallContext } from "../../contexts/CallContext";
import { MessageContextValue, FriendsContextValue, CallContextValue } from "../../ts/types/contextvalue_types";
import { FriendWithAvatar } from "../../ts/types/friends_types";

import Chatheader from "./Chatheader";
import CallWindow from "./callwindow/CallWindow";
import Chat from "./Chat";
import Sendfield from "./Sendfield";

const ChatContainer: React.FC = () => {

  const messageContext: MessageContextValue = useMessageContext();
  const friendsContext: FriendsContextValue = useFriendsContext();
  const callContext: CallContextValue = useCallContext();

  const [friend, setFriend] = useState<FriendWithAvatar | undefined>(undefined);

  useEffect(() => {
    /**
     * @private
     */
    const currentFriend: FriendWithAvatar | undefined = friendsContext.getFriend(messageContext.showMessagesFromUserId);
    setFriend(currentFriend);
  }, [messageContext.showMessagesFromUserId, friendsContext.friends]);

  return(
    <div className="chatcontainer">

      { // TODO remove the exclamation mark
	(callContext.inCallWith === friend?.friendId) ?
	<CallWindow friend={ friend! } /> 
	:
	<Chatheader friend={ friend } />
      }

      <div className="chatcontainer__main">
	<Chat friend={ friend } />
	<Sendfield friend={ friend } />
      </div>

    </div>
  );
}

export default ChatContainer;
