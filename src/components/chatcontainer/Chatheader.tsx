import React from "react";
import "./Chatheader.scss";

import { ReactComponent as PhoneCall } from "../../assets/feathericons/phone-call.svg";
import { ReactComponent as Video } from "../../assets/feathericons/video.svg";
import { ReactComponent as AtSign } from "../../assets/feathericons/at-sign.svg";
import { ReactComponent as Search } from "../../assets/feathericons/search.svg";

import { NotificationType } from "../../ts/types/notification_types";
import { FriendWithAvatar } from "../../ts/types/friends_types";
import { TinyInt } from "../../ts/types/mariadb_types";
import { useMessageContext } from "../../contexts/MessageContext";
import { useNotificationContext } from "../../contexts/NotificationContext";
import { useModalContext } from "../../contexts/ModalContext";
import { useFriendsContext } from "../../contexts/FriendsContext";
import { useUserContext } from "../../contexts/UserContext";
import { useCallContext } from "../../contexts/CallContext";
import { MessageContextValue,
	 NotificationContextValue,
	 ModalContextValue,
	 FriendsContextValue,
	 UserContextValue,
	 CallContextValue } from "../../ts/types/contextvalue_types";
import { CallingType } from "../../ts/types/call_types";

import Tooltip, { TooltipPlacement } from "../Tooltip";

interface Props {
  friend: FriendWithAvatar | undefined;
}

const Chatheader: React.FC<Props> = (props: Props) => {

  const messageContext: MessageContextValue = useMessageContext();
  const notifContext: NotificationContextValue = useNotificationContext();
  const modalContext: ModalContextValue = useModalContext();
  const friendsContext: FriendsContextValue = useFriendsContext();
  const userContext: UserContextValue = useUserContext();
  const callContext: CallContextValue = useCallContext();

  const handleVoiceClick = () => {
    if (!props.friend) return;
    callContext.startCalling(props.friend.friendId, CallingType.Audio);
  };

  const handleVideoClick = () => {
    if (!props.friend) return;
    callContext.startCalling(props.friend.friendId, CallingType.Video);
  }; 

  const handleSearchClick = () => messageContext.setShowSearchMessages(!messageContext.showSearchMessages);

  const showProfile = () => {
    /**
     * @private
     */
    modalContext.setShowProfileModal(true);
    modalContext.setShowProfileId(messageContext.showMessagesFromUserId);
  };

  const handleName = () => {
    /**
     * @private
     */   
    if (!props.friend) return "unknown";
    return props.friend.friendName;
  };

  const isFriend: () => boolean = () => {
    if (!props.friend) return false;
    if (props.friend.blocked === TinyInt.TRUE) return false; 
    return true;
  };

  return(
    <header className="chatheader" >

      { /* Leftside (Avatar @ username) */ }
      <div className="chatheader__leftside" onClick={ showProfile }>
        <img
	  className={ "chatheader__leftside__avatar " + ((props.friend) ? userContext.handleStatusClasses(props.friend.status) : "status--offline") }
	  alt="pic"
	  src={ friendsContext.getAvatar(props.friend) } />
        <span className="chatheader__leftside__at"><AtSign /></span>
        <div className="chatheader__leftside__username">{ handleName() }</div>
      </div>

      { /* Controls/Buttons (Voice & Video & Search) but only if friends */ }
      {
	(isFriend()) ?
	<div className="chatheader__rightside">
          <Tooltip title="Start&nbsp;Voice&nbsp;Call" placement={ TooltipPlacement.BOTTOM }>
            <PhoneCall
              className="chatheader__rightside__btn-voice"
              onClick={ handleVoiceClick }
            />
          </Tooltip>

          <Tooltip title="Start&nbsp;Video&nbsp;Call" placement={ TooltipPlacement.BOTTOM }>
            <Video
              className="chatheader__rightside__btn-video"
              onClick={ handleVideoClick }
            />
          </Tooltip>

	  <Search className="chatheader__rightside__btn-search" onClick={ handleSearchClick } />
	</div>
	:
	null
      }

    </header>
  );
};

export default Chatheader;
