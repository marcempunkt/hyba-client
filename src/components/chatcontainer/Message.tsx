import React, { useState, useEffect, MouseEvent  } from "react";
import "./Message.scss";

import { ReactComponent as Trash } from "../../assets/feathericons/trash-2.svg";
import { ReactComponent as Copy } from "../../assets/feathericons/copy.svg";

import { useModalContext } from "../../contexts/ModalContext";
import { useContextmenuContext } from "../../contexts/ContextmenuContext";
import { useUserContext } from "../../contexts/UserContext";
import { useAppContext } from "../../contexts/AppContext";
import { ModalContextValue, ContextmenuContextValue, UserContextValue, AppContextValue } from "../../ts/types/contextvalue_types";
import { FriendWithAvatar } from "../../ts/types/friends_types";
import { TinyInt } from "../../ts/types/mariadb_types";

import ContextmenuItem from "../contextmenu/ContextmenuItem";

interface Props {
  friend: FriendWithAvatar | undefined;
  msgId: number;
  fromUser: number;
  msgContent: string;
  sendAt: number;
  grouped: boolean;
}

const Message: React.FC<Props> = (props: Props) => {
  /**
   * Renders a Message
   * @component
   */
  const modalContext: ModalContextValue = useModalContext();
  const contextmenuContext: ContextmenuContextValue = useContextmenuContext();
  const userContext: UserContextValue = useUserContext();
  const appContext: AppContextValue = useAppContext();

  const [author, setAuthor] = useState<string>("");
  const [content, setContent] = useState<Array<string | JSX.Element>>([]);

  const getAvatarUrl: () => string = () => {
    /* default avatar */
    const defaultAvatar: string = `${appContext.API_URL}/users/get/avatar/0`;
    /* I sent the message */
    if (props.fromUser === userContext.loggedInUserId) return userContext.avatar;
    /* Your friend sent the message */
    /* but is blocked */
    if (props.friend && props.friend.blocked === TinyInt.TRUE) return defaultAvatar; 
    if (props.friend) return props.friend.avatar;
    /* fall through case */
    return defaultAvatar;
  };

  const handleTrashBtnClick = () => {
    modalContext.setDeleteMessageId(props.msgId);
    modalContext.setShowDeleteMessageModal(true);
  };

  const handleCopy = () => {
    const data = `${author} (${appContext.handleDate(props.sendAt)}): ${props.msgContent}`
    navigator.clipboard.writeText(data)
             .then(() => console.log("copied: ", data))
             .catch((err) => console.log(err));
  };

  const copyMenuItem = <ContextmenuItem
			 key={ 1 }
			 icon={ <Copy /> }
			 description="Copy"
			 handleClick={ handleCopy }/>;

  const deleteMenuItem = <ContextmenuItem
                           key={ 2 }
                           icon={ <Trash /> }
                           description="Delete"
                           handleClick={ handleTrashBtnClick }/>;

  const handleContextmenu = (e: MouseEvent<HTMLDivElement>) => {
    e.preventDefault();

    contextmenuContext.create(e.pageX, e.pageY);

    contextmenuContext.setComponents([
      copyMenuItem,
      deleteMenuItem
    ]);
  };

  useEffect(() => {
    /**
     * Get the author of a message
     * @effect
     */
    if (props.fromUser === userContext.loggedInUserId) return setAuthor(userContext.loggedInUsername);
    if (!props.friend) return setAuthor("unknown");
    return setAuthor(props.friend.friendName);
  }, [props.friend]);

  useEffect(() => {
    /**
     * Check if content has a link in it
     * @effect
     */
    const linkifiedContent: Array<string | JSX.Element> = appContext.linkify(props.msgContent);
    setContent(linkifiedContent);
  }, [props.msgContent]);

  return(
    <div className={ "message" + ((props.fromUser === userContext.loggedInUserId) ? " message--me" : "") +
		    ((props.grouped) ? " message--grouped" : "") }>
      <img
	alt={ `${props.fromUser}` }
	src={ getAvatarUrl() }
	className="message__avatar" />

      <div className="message__bubble" onContextMenu={ handleContextmenu } >
	<pre className="message__bubble__content">
	  { ...content }
	</pre>
	<span className="message__bubble__date">{ appContext.handleDate(props.sendAt) }</span>
      </div>
    </div>
  );
};

export default Message;
