import React, { MouseEvent } from "react";
import "./SearchMessages.scss";

import { MessageContextValue } from "../../ts/types/contextvalue_types";
import { useMessageContext } from "../../contexts/MessageContext";

import CloseButton from "../../ui/CloseButton";

const SearchMessages: React.FC = () => {

  const messageContext: MessageContextValue = useMessageContext();

  const handleCloseClick = () => messageContext.setShowSearchMessages(false);

  const handlePropagation = (e: MouseEvent<HTMLDivElement>) => e.stopPropagation();

  return(
    <div className="searchmessages"
	 onClick={ handleCloseClick }>

      <div className="searchmessages__module" onClick={ handlePropagation }>
	<header>
	  <input type="text" placeholder="Search input" />
	  <CloseButton onClick={ handleCloseClick } position="right" />
	</header>

      </div>
      
    </div>
  );
};

export default SearchMessages;
