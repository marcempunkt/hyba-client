import React, { ChangeEvent, useEffect } from "react";

import { useCallContext } from "../../../contexts/CallContext";
import { CallContextValue } from "../../../ts/types/contextvalue_types";

const VolumeSlider: React.FC = () => {

  const callContext: CallContextValue = useCallContext();

  const handleVolumeChange = (e: ChangeEvent<HTMLInputElement>) => {
    e.stopPropagation(); // just in case
    const newVolume: number = Number(e.currentTarget.value); // 0.1 - 200
    if (!Number.isInteger(newVolume) || !callContext.remoteStream) return;
    callContext.setRemoteStreamVolume(newVolume);
  };

  useEffect(() => {
    if (callContext.remoteStreamGainNode) {
      callContext.remoteStreamGainNode.gain.value = callContext.remoteStreamVolume / 100;
    }
  }, [callContext.remoteStreamVolume]);

  return(
    <input
      type="range"
      min="1"
      max="200"
      value={ callContext.remoteStreamVolume }
      onChange={ handleVolumeChange }
    />
  );
};

export default VolumeSlider;
