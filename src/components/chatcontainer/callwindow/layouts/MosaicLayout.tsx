import React, { useState, useEffect, MouseEvent } from "react";
import "./MosaicLayout.scss";

import { ReactComponent as MicOff } from "../../../../assets/feathericons/mic-off.svg";

import { FriendWithAvatar } from "../../../../ts/types/friends_types";
import { useCallContext } from "../../../../contexts/CallContext";
import { useUserContext } from "../../../../contexts/UserContext";
import { useAppContext } from "../../../../contexts/AppContext";
import { useContextmenuContext } from "../../../../contexts/ContextmenuContext";
import { CallContextValue,
	 UserContextValue,
	 AppContextValue,
	 ContextmenuContextValue } from "../../../../ts/types/contextvalue_types";

import ContextmenuItem from "../../../contextmenu/ContextmenuItem";
import VolumeSlider from "../VolumeSlider";
import WebcamVideo from "../WebcamVideo";
import ScreenshareVideo from "../ScreenshareVideo";
import MosaicTile from "./MosaicTile";

interface Props {
  participants: Array<FriendWithAvatar>;
}

const MosaicLayout: React.FC<Props> = (props: Props) => {
  /**
   * Render the Profile Pictures of all participents unless one or all of them
   * is streaming his webcam then replace it with the stream!
   * @component
   * 
   * TODO FIXME this component is in a weird hybrid state
   * it takes an array of participants to be able to be used in group calls
   * but there is only one remote stream and one remote screenshare stream
   * bcause in that department I haven't implemented the group call feature
   */
  const userContext: UserContextValue = useUserContext();
  const appContext: AppContextValue = useAppContext();
  const callContext: CallContextValue = useCallContext();
  const contextmenuContext: ContextmenuContextValue = useContextmenuContext();

  const [focusedTile, setFocusedTile] = useState<number>(0);

  const handleTileClick = (tileId: number) => setFocusedTile((focusedTile === tileId) ? 0 : tileId);

  const getMosaicTileClasses: (tileId: number) => string = (tileId) => {
    if (focusedTile === 0) return ""; 
    if (focusedTile === tileId) return "mosaictile--focused";
    if (focusedTile !== tileId) return "mosaictile--hide";
    return "";
  };

  const handleMute = () => {};

  const muteMenuItem = <ContextmenuItem
			 key={ 1 }
			 icon={ <MicOff/> }
			 description="Mute"
			 handleClick={ handleMute }/>;

  const volumeSlideMenuItem = <div className="contextemenu__item"><VolumeSlider /></div>;

  const handleContextmenuLocal = (e: MouseEvent<HTMLDivElement>) => {
    e.preventDefault();
    contextmenuContext.create(e.pageX, e.pageY);
    contextmenuContext.setComponents([muteMenuItem, volumeSlideMenuItem]);
  };

  const handleContextmenuRemote = (e: MouseEvent<HTMLDivElement>) => {
    e.preventDefault();
    contextmenuContext.create(e.pageX, e.pageY);
    contextmenuContext.setComponents([muteMenuItem, volumeSlideMenuItem]);
  };

  return(
    <div className="mosaiclayout" onContextMenu={ handleContextmenuLocal }>
      { /* Clients webcam/avatar */ }
      <MosaicTile
	nametag={ userContext.loggedInUsername }
	onClick={ () => handleTileClick(1) }
	className={ getMosaicTileClasses(1) }>
	
	<WebcamVideo stream={ callContext.localStream } avatar={ userContext.avatar } />
      </MosaicTile>

      {
	/* Clients screenshare stream */
	(callContext.localScreenshareStream) ?
	<MosaicTile
	  onClick={ () => handleTileClick(2) }
	  className={ getMosaicTileClasses(2) }>
	  <ScreenshareVideo stream={ callContext.localScreenshareStream } />
	</MosaicTile>
	:
	null
      }
      
      {
	/* Remote/s */
	props.participants.map((participant: FriendWithAvatar, index: number) => (
	  // return webcam/avatar tile && screenshare tile if neccessary
	  <React.Fragment key={ participant.friendId }>
	    <MosaicTile
	      nametag={ participant.friendName }
	      onContextMenu={ handleContextmenuRemote }
	      onClick={ () => handleTileClick(index + 3) }
	      className={ getMosaicTileClasses(index + 3) }>
	      <WebcamVideo stream={ callContext.remoteStream }  avatar={ participant.avatar } />
	    </MosaicTile>
	    {
	      (callContext.remoteScreenshareStream) ?
	      <MosaicTile
		onClick={ () => handleTileClick(index + 4) }
		className={ getMosaicTileClasses(index + 4) }>
		<ScreenshareVideo stream={ callContext.remoteScreenshareStream } />
	      </MosaicTile>
	      :
	      null
	    }
	  </React.Fragment>
	))
      }
    </div>
  );
};

export default MosaicLayout;
