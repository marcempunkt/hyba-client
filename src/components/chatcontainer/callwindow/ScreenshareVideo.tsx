import React, { useState, useEffect } from "react";
import { v4 as uuidv4 } from "uuid";
import "./ScreenshareVideo.scss";

interface Props {
  stream: MediaStream;
}

const ScreenshareVideo: React.FC<Props> = (props: Props) => {

  const [id] = useState<string>("a" + uuidv4().split("-").join(""));

  useEffect(() => {
    const localVideo: HTMLMediaElement | null = document.querySelector("#" + id);
    if (localVideo) { localVideo.srcObject = props.stream; }

    return () => {
      if (!localVideo) return;
      localVideo.pause();
      localVideo.srcObject = null;
      localVideo.load();
    }
  }, [props.stream, id]);

  return(
    <video
      className="screensharevideo"
      id={ id }
      playsInline
      autoPlay
      muted >
    </video>
  );
};

export default ScreenshareVideo;
