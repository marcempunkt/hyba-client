import React, { useState, useRef, useEffect, MouseEvent } from "react";
import "./CallWindow.scss";

import { FriendWithAvatar } from "../../../ts/types/friends_types";
import { SocketState } from "../../../ts/types/socket_types";
import { Layout } from "../../../ts/types/call_types";
import { SocketContextValue, CallContextValue } from "../../../ts/types/contextvalue_types";
import { useSocketContext } from "../../../contexts/SocketContext";
import { useCallContext } from "../../../contexts/CallContext";

import MosaicLayout from "./layouts/MosaicLayout";
import LivestreamLayout from "./layouts/LivestreamLayout";
import CallWindowControls from "./CallWindowControls";
import ConnectionLostIndicator from "./ConnectionLostIndicator";
import LayoutPicker from "./LayoutPicker";

interface Props {
  friend: FriendWithAvatar;
}

const CallWindow: React.FC<Props> = (props: Props) => {

  const socketContext: SocketContextValue = useSocketContext();
  const callContext: CallContextValue = useCallContext();

  const [showScreensharePickerModal, setShowScreensharePickerModal] = useState<boolean>(true);
  const [showUI, setShowUI] = useState<boolean>(true);
  const [mouseDidntMove, setMouseDidntMove] = useState<number>(0);
  const hideControlsAfterSec: number = 3;

  const callwindow = useRef<HTMLDivElement | null>(null);

  const handleMousemove = (_event: MouseEvent<HTMLDivElement>) => {
    if (mouseDidntMove) {
      setShowUI(true);
      setMouseDidntMove(0);
    }
  };

  const renderConnectionLostIndicator: () => JSX.Element | undefined = () => {
    if (socketContext.state === SocketState.Disconnected) return <ConnectionLostIndicator />;
  };

  const renderLayout: () => JSX.Element = () => {
    switch (callContext.callWindowLayout) {
      case Layout.Mosaic: {
	return <MosaicLayout participants={ [props.friend] } />;
      }
      case Layout.Livestream: {
	return <LivestreamLayout participants={ [props.friend] } />;
      }
      default: {
	return <MosaicLayout participants={ [props.friend] } />;
      }
    }
  };

  useEffect(() => {
    /**
     * Hide Controls after a certain time the mouse hasn't been moved by the user
     * inside the CallWindow Component
     */
    if (mouseDidntMove >= hideControlsAfterSec) {
      setShowUI(false);
    }
  }, [mouseDidntMove]);

  useEffect(() => {
    /**
     * Record the seconds the mouse did not move 
     * @effect
     */
    const interval = setInterval(
      () => {
	if (mouseDidntMove < hideControlsAfterSec) {
	  setMouseDidntMove(mouseDidntMove + 1);
	}
      },
      1000);

    return () => clearInterval(interval);
  }, [mouseDidntMove]);

  return(
    <header className="callwindow" ref={ callwindow } onMouseMove={ handleMousemove } >
      <LayoutPicker show={ showUI } />
      { renderConnectionLostIndicator() }
      { renderLayout() }
      <CallWindowControls callwindowRef={ callwindow } show={ showUI } />
    </header>
  );
};

export default CallWindow;
