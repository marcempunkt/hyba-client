import React, { useState, useEffect, useRef, ChangeEvent, FormEvent } from "react";
import axios, { AxiosResponse, AxiosError } from "axios";
import logger from "../../utils/logger";
import "./CreatePost.scss";

import { ReactComponent as Image } from "../../assets/feathericons/image.svg";
import { ReactComponent as Feather } from "../../assets/feathericons/feather.svg";

import { usePostContext } from "../../contexts/PostContext";
import { useUserContext } from "../../contexts/UserContext";
import { useAppContext } from "../../contexts/AppContext";
import { useSocketContext } from "../../contexts/SocketContext";
import { useNotificationContext } from "../../contexts/NotificationContext";
import type { PostContextValue,
	      UserContextValue,
	      NotificationContextValue,
	      AppContextValue,
	      SocketContextValue } from "../../ts/types/contextvalue_types";
import type { Response } from "../../ts/types/axios_types";
import { NotificationType } from "../../ts/types/notification_types";
import { Post, PostsReducerActionType } from "../../ts/types/post_types";
import { SocketState } from "../../ts/types/socket_types";

import CloseButton from "../../ui/CloseButton";
import CreatePostGallery from "./CreatePostGallery";

const CreatePost: React.FC = () => {

    const postContext: PostContextValue = usePostContext();
    const userContext: UserContextValue = useUserContext();
    const notifContext: NotificationContextValue = useNotificationContext();
    const appContext: AppContextValue = useAppContext();
    const socketContext: SocketContextValue = useSocketContext();

    const [submitting, setSubmitting] = useState<boolean>(false);

    const inputFile = useRef<HTMLInputElement>(null);
    const formRef = useRef<HTMLFormElement>(null);

    const handleFileChange = async (e: ChangeEvent<HTMLInputElement>) => {
        if (e.currentTarget.files) {
            const images: Array<string> = await Promise.all(appContext.getFileUrls(e.currentTarget.files));
            postContext.setNewPostImages(images);
        }
    };

    const handleClose = () => {
        postContext.setNewPostContent("");
        postContext.setNewPostImages([]);
        postContext.setShowCreatePost(false); 
    }

    const handleAddImageClick = () => {
        if (inputFile && inputFile.current) {
            inputFile.current.click();
        }
    };

    const handleDisableSubmitBtn: () => boolean = () => {
        if (postContext.newPostContent === "" && postContext.newPostImages.length === 0) return true;
        return false;
    }

    const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault();

        // FIXME TODO make ui component to represent how many characters the client has left/written
        if (postContext.newPostContent.length > 200) {
            return notifContext.create(NotificationType.ERROR, "Your post is longer than 200 characters.");
        }
        /* Post has no meaningful value */
        if (postContext.newPostContent === "" && postContext.newPostImages.length === 0) {
            return notifContext.create(NotificationType.ERROR, "Please enter some text or add some images to your post");
        }
        if (!inputFile || !inputFile.current || !inputFile.current.files) return;

        /* Enter submitting mode */
        setSubmitting(true);

        if (socketContext.state === SocketState.Disconnected) {
            setTimeout(() => { if (formRef?.current) { formRef.current.requestSubmit(); } }, 500);
            return logger.error.blue("CreatePost", "Currently disconnected from SocketServer. Retrying in 500ms.");
        }

        const formData: FormData = new FormData();
        [...inputFile.current.files].forEach((image: File) => formData.append("images", image));
        formData.append("content", `${postContext.newPostContent}`);
        formData.append("token", userContext.token!);

        /* upload the post */
        axios.post(
            `${appContext.API_URL}/posts/`,
            formData,
            { headers: {
                "Content-Type": "multipart/form-data",
                Authorization: `Bearer: ${userContext.token}`,
            } }
        ).then((res: AxiosResponse<Post>) => {
	    const newPost: Post = res.data;
	    postContext.dispatchPosts({ type: PostsReducerActionType.ADD, payload: { newPost } });
	    /* Reset create post */
	    postContext.setNewPostContent("");
	    postContext.setNewPostImages([]);
	    postContext.setShowCreatePost(false);
	    /* emit socket event */
	    socketContext.maybeReconnect();
	    if (socketContext.socket) {
	        logger.log.blue("CreatePost", "Socket event emitted send_post");
	        socketContext.socket.emit("send_post", userContext.loggedInUserId, newPost); 
	    }
	    setSubmitting(false);
        }).catch((err: AxiosError<Response>) => {
            switch (err.message) {
	        case "Network Error": {
	            setTimeout(() => { if (formRef?.current) { formRef.current.requestSubmit(); } }, 500);
	            return logger.error.blue("CreatePost", "Network Error posting the new post. Retrying in 500ms.");
	        }
	        default: {
	            console.error(err);
	            if (err.response) { notifContext.create(NotificationType.ERROR, err.response.data.message); }
	            setSubmitting(false);
	        }
            }
        })
    };

    const handleTextareaChange = (e: ChangeEvent<HTMLTextAreaElement>) => {
        postContext.setNewPostContent(e.currentTarget.value)
    };

    return(
        <form className={ "createpost" + ((submitting) ? " createpost--submitting" : "") } onSubmit={ handleSubmit } ref={ formRef }>
            <CloseButton className="createpost__closebutton" onClick={ handleClose } position="right" />

            <div className={ "createpost__main" + ((postContext.newPostImages.length) ? "--with-gallery" : "") }>
	        <header className="createpost__main__header">
	            <img className="createpost__main__header__userimg" src={ userContext.avatar } />
	            <span className="createpost__main__header__username">{ userContext.loggedInUsername }</span>
	            <span className="createpost__main__header__date">· today</span>
	        </header>

	        {
	            /* preview images */
	            (postContext.newPostImages.length)
	            ?
	            <CreatePostGallery images={ postContext.newPostImages } />
	            :
	            null
	        }

	        <textarea
	            className="createpost__main__textarea"
	            placeholder="Whats happening?"
	            onChange={ handleTextareaChange }
	            value={ postContext.newPostContent }
	            disabled={ submitting } />
	        <hr/>
            </div>

            <footer className="createpost__footer">
	        <button
	            className="btn btn--with-svg margin-right"
	            type="button"
	            onClick={ handleAddImageClick }
	            disabled={ submitting } >
	            <Image />Add images
	        </button>
	        <input
	            type="file"
	            multiple
	            style={{ display: "none" }}
	            accept="image/x-png,image/gif,image/jpeg"
	            ref={ inputFile }
	            onChange={ handleFileChange }
	            disabled={ submitting } />

	        <button
	            className="btn--primary btn--with-svg"
	            type="submit"
	            disabled={ submitting } >
	            <Feather />Post
	        </button>
            </footer>

        </form>
    );
};

export default CreatePost;
