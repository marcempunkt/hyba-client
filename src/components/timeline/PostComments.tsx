import React, { useState, useEffect } from "react";
import "./PostComments.scss";

import { PostComment } from "../../ts/types/post_types";

import Comment from "./Comment";

interface Props {
  comments: Array<PostComment>;
  postId: number;
}

const PostComments: React.FC<Props> = (props: Props) => {

  const [comments, setComments] = useState<Array<PostComment>>([]);
  const [showComments, setShowComments] = useState<boolean>(false);

  const handleShowComments = () => setShowComments(!showComments);

  useEffect(() => {
    let sortedComments: Array<PostComment> = [...props.comments].sort((x: PostComment, y: PostComment) => y.writtenAt - x.writtenAt);
    /* convert to Set to remove duplicates because of bug that sometimes duplicates a comment send to the client
     * when before a remove_comment socket event has been triggered */
    const sortedCommentsSet: Set<PostComment> = new Set(sortedComments);
    sortedComments = Array.from(sortedCommentsSet);
    setComments(sortedComments);
  }, [props.comments])

  return(
    <section className="post__comments">
      <button onClick={ handleShowComments } className="post__comments__showbutton" >
	{ (showComments) ? `Hide comments` : `Show comments` }
      </button>
      {
	(showComments) ?
	<ul className="post_comments__list">
	  { 
	    comments.map((comment: PostComment) => <Comment key={ comment.commentId }
							    commentId={ comment.commentId }
							    postId={ props.postId }
							    author={ comment.userId }
							    content={ comment.content }
							    writtenAt={ comment.writtenAt } />)
	  }
	</ul>
	:
	null
      }
    </section>
  );
};

export default PostComments;
