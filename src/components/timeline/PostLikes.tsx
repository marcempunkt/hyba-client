import React, { useState, useEffect } from "react";
import "./PostLikes.scss";

import { ReactComponent as Heart } from "../../assets/feathericons/heart.svg";

import { useUserContext } from "../../contexts/UserContext";
import { usePostContext } from "../../contexts/PostContext";
import { UserContextValue, PostContextValue } from "../../ts/types/contextvalue_types";

interface Props {
  postId: number;
  likes: Array<number>;
}

const PostLikes: React.FC<Props> = (props: Props) => {

  const userContext: UserContextValue = useUserContext();
  const postContext: PostContextValue = usePostContext();

  // FIXME TODO don't let the user spam the like button
  // instead make the button disabled on click and when successfull http request enable it again

  return(
    <section className="post__like-section">
      <button
	className={ "post__like-section__heartbutton" + ((props.likes.includes(userContext.loggedInUserId) ? "--liked" : "")) }
	onClick={ () => postContext.handleLike(props.postId, userContext.loggedInUserId) } ><Heart /></button>
      <span className="post__like-section__likes">{ props.likes.length }</span>
    </section>
  );
};

export default PostLikes;
