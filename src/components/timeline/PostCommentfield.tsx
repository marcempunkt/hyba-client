import React, { useState, useRef, ChangeEvent, FormEvent } from "react";
import axios, { AxiosResponse, AxiosError } from "axios";
import logger from "../../utils/logger";
import "./PostCommentfield.scss";

import { ReactComponent as Send } from "../../assets/feathericons/send.svg";

import { useAppContext } from "../../contexts/AppContext";
import { useUserContext } from "../../contexts/UserContext";
import { usePostContext } from "../../contexts/PostContext";
import { useNotificationContext } from "../../contexts/NotificationContext";
import { useSocketContext } from "../../contexts/SocketContext";
import { AppContextValue,
	 UserContextValue,
	 PostContextValue,
	 NotificationContextValue,
	 SocketContextValue } from "../../ts/types/contextvalue_types";
import { NotificationType } from "../../ts/types/notification_types";
import { PostsReducerActionType, PostComment } from "../../ts/types/post_types";
import { SocketState } from "../../ts/types/socket_types";

interface Props {
    postId: number;
}

const PostCommentfield: React.FC<Props> = (props: Props) => {

    const appContext: AppContextValue = useAppContext();
    const userContext: UserContextValue = useUserContext();
    const postContext: PostContextValue = usePostContext();
    const notifContext: NotificationContextValue = useNotificationContext();
    const socketContext: SocketContextValue = useSocketContext();

    const [comment, setComment] = useState<string>("");
    const [submitting, setSubmitting] = useState<boolean>(false);

    const formRef = useRef<HTMLFormElement>(null);

    const handleCommentChange = (e: ChangeEvent<HTMLTextAreaElement>) => setComment(e.currentTarget.value);

    const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault();

        if (!comment) return notifContext.create(NotificationType.ERROR, "Type in a comment first!");
        if (comment.length > 255) return notifContext.create(NotificationType.ERROR, `You comment exeeds the characater limit of 255.`);

        setSubmitting(true);

        if (socketContext.state === SocketState.Disconnected) {
            setTimeout(() => { if (formRef?.current) { formRef.current.requestSubmit(); } }, 500);
            return logger.error.blue("PostCommentfield", "Currently disconnected from SocketServer. Retrying in 500ms.");
        }

        const body = {
            token: userContext.token,
            postId: props.postId,
            content: comment,
        };

        axios.post(
            `${appContext.API_URL}/posts/comment`,
            { postId: props.postId, content: comment },
            { headers: { Authorization: `Bearer: ${userContext.token}` } }
        ).then((res: AxiosResponse<PostComment>) => {
	    const newComment: PostComment = res.data;
	    postContext.dispatchPosts({ type: PostsReducerActionType.ADD_COMMENT, payload: { postId: props.postId, newComment } });

	    socketContext.maybeReconnect();
	    if (socketContext.socket) {
	        logger.log.blue("PostCommentfield", "Socket event emitted comment_post");
	        socketContext.socket.emit("comment_post", userContext.loggedInUserId, props.postId, newComment);
	    }
	    setComment("");
	    setSubmitting(false);
	}).catch((err: AxiosError<Response>) => {
	    switch (err.message) {
	        case "Network Error": {
	            setTimeout(() => { if (formRef?.current) { formRef.current.requestSubmit(); } }, 500);
	            return logger.error.blue("PostCommentfield", "Network Error when posting comment. Retrying in 500ms.");
	        }
	        default: {
	            setSubmitting(false);
	            logger.error.blue("PostCommentfield", "Error when posting comment");
	            return console.error(err);
	        }
	    }
	});
    };

    const keyHandler = (e: React.KeyboardEvent<HTMLTextAreaElement>) => {
        /**
         * Eventlistener for textarea
         * On 'Enter' submit the form
         * On 'Shift + Enter' insert breakline
         */
        const sendfield: HTMLFormElement = document.querySelector("#commentfield")!;

        switch(e.key) {
            case "Enter":
	        if (!e.shiftKey) {
	            e.preventDefault();
	            if (comment) return sendfield.requestSubmit();
	        }
        }
    };

    return(
        <form id="commentfield"
	      className={"post__commentfield" + ((submitting) ? " post__commentfield--submitting" : "") }
	      onSubmit={ handleSubmit } ref={ formRef }>

            <textarea
	        className="post__commentfield__textarea"
	        placeholder={ `Write a reply...` }
                onChange={ handleCommentChange }
                onKeyDown={ keyHandler }
                value={ comment }
	        disabled={ submitting } />

            <button className="post__commentfield__submit-btn" type="submit" disabled={ submitting }><Send /></button>
        </form>
        
    );
};

export default PostCommentfield;
