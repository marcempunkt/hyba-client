import React from "react";
import "./PostHeader.scss";

import { usePostContext } from "../../contexts/PostContext";
import { useUserContext } from "../../contexts/UserContext";
import { useModalContext } from "../../contexts/ModalContext";
import { PostContextValue, UserContextValue, ModalContextValue } from "../../ts/types/contextvalue_types";
import { FriendWithAvatar } from "../../ts/types/friends_types";

import DefaultImage from "../../assets/images/avatar.png";

interface Props {
  friend: FriendWithAvatar | undefined;
  writtenBy: number;
  writtenAt: number;
}

const PostHeader: React.FC<Props> = (props: Props) => {

  const postContext: PostContextValue = usePostContext();
  const userContext: UserContextValue = useUserContext();
  const modalContext: ModalContextValue = useModalContext();

  const handleProfileImageClick: () => void = () => {
    if (props.writtenBy === userContext.loggedInUserId) return;
    modalContext.setShowProfileId(props.writtenBy);
    modalContext.setShowProfileModal(true);
  };

  const getImageSource: () => string = () => {
    /**
     * render default image or the user's profile image
     * @private
     */
    if (props.writtenBy === userContext.loggedInUserId) return userContext.avatar;
    if (!props.friend) return DefaultImage;
    return props.friend.avatar;
  };

  return(
    <header className="post__header">
      <img
	className="post__header__userimg"
	style={ (props.writtenBy === userContext.loggedInUserId) ? { cursor: "default" } : { } }
	src={ getImageSource() }
	alt={ (props.friend) ? props.friend.friendName : userContext.loggedInUsername }
	onClick={ handleProfileImageClick } />
      <span className="post__header__username">{ (props.friend) ? props.friend.friendName : userContext.loggedInUsername }</span>
      <span className="post__header__date">{ `· ${postContext.handleDate(props.writtenAt)}` }</span>
    </header>
  );
};

export default PostHeader;
