import React from "react";
import "./PostGallery.scss";

import { useAppContext } from "../../contexts/AppContext";
import { useModalContext } from "../../contexts/ModalContext";
import { AppContextValue, ModalContextValue } from "../../ts/types/contextvalue_types";

interface Props {
    images: Array<string>;
}

const PostGallery: React.FC<Props> = (props: Props) => {

    const appContext: AppContextValue = useAppContext();
    const modalContext: ModalContextValue = useModalContext();

    const handleImageClick: (imgSrc: string) => void = (imgSrc) => {
        modalContext.setImageModalSrc(`${appContext.API_URL}/posts/image/` + imgSrc);
        modalContext.setShowImageModal(true);
    };

    /* if there are none images it won't be rendered at all */
    if (!props.images.length) return null
    return(
        <div className="post__content__gallery">
            {
	        props.images.map((image: string) => {
	            return(
	                <div
	                    key={ image }
	                    onClick={ () => handleImageClick(image) }
	                    className="post__content__gallery__img"
                            style={ { backgroundImage: `url(${appContext.API_URL + "/posts/image/"}${image})` } }>
                            
                        </div>
	            );
	        })
            }
        </div>
    );
};

export default PostGallery;
