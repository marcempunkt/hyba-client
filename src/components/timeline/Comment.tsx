import React, { useState, useEffect, MouseEvent } from "react";
import axios, { AxiosResponse, AxiosError } from "axios";
import logger from "../../utils/logger";

import { ReactComponent as Trash } from "../../assets/feathericons/trash-2.svg"

import { useAppContext } from "../../contexts/AppContext";
import { usePostContext } from "../../contexts/PostContext";
import { useUserContext } from "../../contexts/UserContext";
import { useContextmenuContext } from "../../contexts/ContextmenuContext";
import { useNotificationContext } from "../../contexts/NotificationContext";
import { useSocketContext } from "../../contexts/SocketContext";
import { AppContextValue,
	 PostContextValue,
	 UserContextValue,
	 ContextmenuContextValue,
	 NotificationContextValue,
	 SocketContextValue } from "../../ts/types/contextvalue_types";
import { NotificationType } from "../../ts/types/notification_types";
import type { Response } from "../../ts/types/axios_types";
import { PostsReducerActionType } from "../../ts/types/post_types";

import ContextmenuItem from "../contextmenu/ContextmenuItem";

interface Props {
    commentId: number;
    postId: number;
    author: number;
    content: string;
    writtenAt: number;
}

const Comment: React.FC<Props> = (props: Props) => {

    const appContext: AppContextValue = useAppContext();
    const postContext: PostContextValue = usePostContext();
    const userContext: UserContextValue = useUserContext();
    const contextmenuContext: ContextmenuContextValue = useContextmenuContext();
    const notifContext: NotificationContextValue = useNotificationContext();
    const socketContext: SocketContextValue = useSocketContext();

    const [content, setContent] = useState<Array<string | JSX.Element>>([]);
    const [name, setName] = useState<string>("unknown");

    useEffect(() => {
        const CancelToken = axios.CancelToken;
        const source = CancelToken.source();

        userContext.getUsernameById(props.author, source.token)
	           .then((username: string) => setName(username))
	           .catch(() => setName("unknown"));

        return () => source.cancel();
    }, [props.author]);

    useEffect(() => {
        /**
         * linkify content of comment
         * @effect
         */
        const linkifiedContent: Array<string | JSX.Element> = appContext.linkify(props.content);
        setContent(linkifiedContent);
    }, [props.content]);

    const handleDelete = () => {
        axios.delete(
            `${appContext.API_URL}/posts/comment/`,
            {
                data: { postId: props.postId, commentId: props.commentId },
                headers: { Authorization: `Bearer: ${userContext.token}` }
            }
        ).then((res: AxiosResponse<Response>) => {
	    postContext.dispatchPosts({ type: PostsReducerActionType.REMOVE_COMMENT, payload: { postId: props.postId, commentId: props.commentId }});
	    notifContext.create(NotificationType.SUCCESS, res.data.message);
	    socketContext.maybeReconnect();
	    if (socketContext.socket) {
	        logger.log.blue("Comment", "Socket event emitted remove_comment_post");
	        socketContext.socket.emit("remove_comment_post", userContext.loggedInUserId, props.postId, props.commentId); 
	    }
	}).catch((err: AxiosError<Response>) => {
	    console.error(err);
	    if (err.response) return notifContext.create(NotificationType.ERROR, err.response.data.message);
	});
    };

    const deleteMenuItem = <ContextmenuItem key={ 1 } icon={ <Trash /> } description="Delete" handleClick={ handleDelete } />;

    const handleContextmenu = (e: MouseEvent<HTMLPreElement>) => {
        e.preventDefault();
        contextmenuContext.create(e.pageX, e.pageY);
        contextmenuContext.setComponents([deleteMenuItem]);
    };

    return(
        <li className="post__comments__list__comment">
            <span className="post__comments__list__comment__author">{ name }</span>
            <pre className="post__comments__list__comment__content selectable" onContextMenu={ handleContextmenu }>{ ...content }</pre>
            <span className="post__comments__list__comment__date">{ postContext.handleDate(props.writtenAt) }</span>
        </li>
    );
};

export default Comment;
