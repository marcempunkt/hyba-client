const { app, autoUpdater, dialog, BrowserWindow, desktopCapturer, ipcMain } = require("electron");
const path = require("path");
const fs = require("fs");
const os = require("os");
const localshortcut = require("electron-localshortcut");

const isDev = process.env.IS_DEV == "true" ? true : false;

function createWindow() {
  // Create the browser window.
  const mainWindow = new BrowserWindow({
    width: 1200,
    height: 720,
    webPreferences: {
      preload: path.join(__dirname, 'preload.js'),
      nodeIntegration: true,
      contextIsolation: true,
      enableRemoteModule: true,
    },
  });

  mainWindow.removeMenu(null);
  mainWindow.setBackgroundColor("#2e3440");

  /* Hotkeys */
  localshortcut.register(mainWindow, ["Ctrl+R", "Command+R"], () => mainWindow.reload());
  localshortcut.register(mainWindow, ["Ctrl+I", "Command+I"], () => mainWindow.openDevTools());

  if (isDev) {
    mainWindow.loadURL('http://localhost:3000');
  } else {
    mainWindow.loadFile(`${path.join(__dirname, '../../build/index.html')}`);
  }
}

app.whenReady().then(() => {
  // This method will be called when Electron has finished
  // initialization and is ready to create browser windows.
  // Some APIs can only be used after this event occurs.
  createWindow();
  app.on('activate', () => {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (BrowserWindow.getAllWindows().length === 0) return createWindow();
  });

  // Handling Screenshare in electron
  ipcMain.handle("get-screenshare-stream-previews", async () => {
    const inputSources = await desktopCapturer.getSources({
      types: ["window", "screen"],
      thumbnailSize: { width: 320, height: 240 },
    });

    const previewSources = inputSources.map((source) => {
      return({
	id: source.id,
	name: source.name,
	thumbnail: source.thumbnail.toDataURL(),
      });
    });

    return previewSources;
  });
});

app.on('window-all-closed', () => {
  // Quit when all windows are closed, except on macOS. There, it's common
  // for applications and their menu bar to stay active until the user quits
  // explicitly with Cmd + Q.
  if (process.platform !== 'darwin') return app.quit();
});


// console.log({ appGetVersion: app.getVersion() });
// console.log({ appGetAppPath: app.getPath("appData") });

// ------------------------------------------------------------------------------------- Electron Updater
/* if (!isDev) {
 *   autoUpdater.setFeedURL({ url: `https://download.babbeln.app/${process.platform}/${app.getVersion()}` });
 * 
 *   autoUpdater.on("update-downloaded", (event, releaseNotes, releaseName) => {
 *     const dialogOpts = {
 *       type: "info",
 *       buttons: ["Restart", "Later"],
 *       title: "Babbeln Update",
 *       message: process.platform === "win32" ? releaseNotes : releaseName,
 *       detail: "A new version has been downloaded. Restart the application to apply the updates.",
 *     };
 * 
 *     dialog.showMessageBox(dialogOpts).then((data) => {
 *       if (data.response === 0) return autoUpdater.quitAndInstall();
 *     })
 *   });
 * 
 *   autoUpdater.on("error", (message) => {
 *     console.error("There was a problem updating the application.");
 *     console.error("message");
 *   });
 * 
 *   setInterval(() => autoUpdater.checkForUpdates(), 60000);
 * } */

/*
// My try on a auto updater
app.whenReady().then(() => {
  if (isDev) return;
  return;

  // Get path of asar file & resource dir stored on the system
  const asarPath = app.getAppPath(); // => /tmp/.APPNAMExlsjda345/resources/app.asar/src/electron/
  const resourcesPath = path.join(asarPath, ".."); // => /tmp/.APPNAMExlsjda345/resources/
  console.log({ asarPath });
  console.log({ resourcesPath });

  // TODO: compare current version with newest on webserver
  // TODO: download new version & call it something like "update.asar"
  // TODO: remove app.asar & rename update.asar to app.asar
  // TODO: restart the whole app

  // This is only for testing if I can modify the content of the resources directory
  fs.copyFile(asarPath, path.join(resourcesPath, "update.asar"), (err) => {
    if (err) return console.log({ err }); // throws error, see below!
    console.log("copied: ", fs.readdirSync(resourcesPath)); // if it would be successfull print all the content of resources dir
  });
});
*/
