import React, { useState, useReducer, useContext, useRef, createContext, useEffect } from "react";
import axios, { AxiosResponse, AxiosError } from "axios";
import logger from "../utils/logger";

import { useSocketContext } from "./SocketContext";
import { useUserContext } from "./UserContext";
import { useNotificationContext } from "./NotificationContext";
import { useAppContext } from "./AppContext";
import { useFriendsContext } from "./FriendsContext";
import { FriendrequestsContextValue,
	 SocketContextValue,
	 UserContextValue,
	 NotificationContextValue,
	 AppContextValue,
         FriendsContextValue } from "../ts/types/contextvalue_types";
import { NotificationType } from "../ts/types/notification_types";
import { FriendsReducerActionType,
	 FriendsReducerAction,
	 PendingsReducerActionType,
	 PendingsReducerAction,
	 Friend,
	 FriendWithAvatar,
	 Friendrequest,
	 Status } from "../ts/types/friends_types";
import { Response } from "../ts/types/axios_types";
import { TinyInt } from "../ts/types/mariadb_types";
import { SocketState } from "../ts/types/socket_types";

import DefaultImage from "../assets/images/avatar.png";

export const FriendrequestsContext = createContext<FriendrequestsContextValue | undefined>(undefined);

export const useFriendrequestsContext = () => {
    /**
     * Consume FriendrequestsContext with error handling
     * @public
     *
     * returns {Context}    FriendreqeustsContext
     */
    const context = useContext(FriendrequestsContext);
    if (context === undefined) {
        throw Error( "FriendrequestsContext is undefined! " +
                     "Are you consuming FriendrequestsContext " +
                     "outside of the FriendrequestsContextProvider?" );
    }
    return context;
};

const reducerPendings = (state: Array<Friendrequest>, action: PendingsReducerAction): Array<Friendrequest> => {
    /**
     * reducer function for pending friendrequests.
     * @private
     *
     * @param  {Friendrequest[]}  state
     * @param  {any}              action
     *
     * returns {Friendrequest[]}  state that has been altered by the action
     */
    switch (action.type) {
        case PendingsReducerActionType.FETCH: {
            logger.log.magenta(
                "FriendrequestsContext",
                "PendingsReducerActionType FETCH"
            );
            if (!action.payload) return state;
            return action.payload.fetchedPendings;
        }

        case PendingsReducerActionType.REMOVE: {
            /** remove an pending friendrequest from the pending state */
            logger.log.magenta(
                "FriendrequestsContext",
                "PendingsReducerActionType REMOVE"
            );
            const newState: Array<Friendrequest> = state.filter((pending: Friendrequest) => {
	        return pending.id !== action.payload.pendingId;
            });
            return newState;
        }

        case PendingsReducerActionType.ADD: {
            /** Add a pending friendrequestitem to the pendings state */
            logger.log.magenta(
                "FriendrequestsContext",
                "PendingsReducerActionType ADD"
            );
            const isInState: boolean = state.some((el: Friendrequest) => el.id === action.payload.pending.id);
            if (isInState) return state;
            return [...state, action.payload.pending];
        }

        case PendingsReducerActionType.LOGOUT: {
            logger.log.magenta(
                "FriendrequestsContext",
                "PendingsReducerActionType LOGOUT"
            );
            return [];
        }

        default: {
            logger.error.magenta(
                "FriendrequestsContext",
                `PendingsReducerActionType ${action.type}`
            );
            return state;
        }
    }
};

interface Props {
    children: JSX.Element;
}

const FriendrequestsContextProvider: React.FC<Props> = (props: Props) => {

    const socketContext: SocketContextValue = useSocketContext();
    const userContext: UserContextValue = useUserContext();
    const notifContext: NotificationContextValue = useNotificationContext();
    const appContext: AppContextValue = useAppContext();
    const friendsContext: FriendsContextValue = useFriendsContext();

    const [pendings, dispatchPendings] = useReducer(reducerPendings, []);

    /**
     * In React setTimoute will close all props/state
     * For example if you subscribe to an ID, and later want to unsubscribe, it would be a bug if ID could change over time
     * but in this case we want to have the current/latest/updated value of socketContext.state
     * to break out of the setTimeout loop in sendMessage/deleteGloballyMessage etc.
     * 
     * Source:
     * Cristian Salcescu
     * https://medium.com/programming-essentials/how-to-access-the-state-in-settimeout-inside-a-react-function-component-39a9f031c76f
     * 
     * https://github.com/facebook/react/issues/14010
     */
    const socketStateRef = useRef<SocketState>(socketContext.state)
    socketStateRef.current = socketContext.state;

    useEffect(() => {
        /**
         * Download all friends and pending friendrequests
         * @effect
         */
        fetchPendings();
    }, [userContext.loggedInUserId]);

    const reset = () => {
        /**
         * Resets the FriendsContext State 
         * example: when logging out
         * @public
         */
        dispatchPendings({ type: PendingsReducerActionType.LOGOUT });
    };

    const fetchPendings = () => {
        /* Get all pending friendrequests */
        if (!userContext.token) return logger.error.magenta(
            "FriendsContext",
            "Tried to fetch pending friendrequests (pendings), but no authentication token is available"
        );
        axios.get(
            `${appContext.API_URL}/friendrequests/get`,
            { headers: { Authorization: `Bearer: ${userContext.token}` } }
        ).then((res: AxiosResponse<Array<Friendrequest>>) => {
	    dispatchPendings({ type: PendingsReducerActionType.FETCH, payload: { fetchedPendings: res.data } });
	}).catch((err: AxiosError<Response>) => {
	    switch (err.message) {
	        case "Network Error": {
	            setTimeout(() => fetchPendings(), 1_000);
	            return logger.error.magenta(
                        "FriendsContext",
                        "Network Error when fetching pending Friendrequests. Retrying in 1000ms."
                    );
	        }
	        default: {
	            logger.error.magenta(
                        "FriendsContext",
                        "Error when fetching pending Friendrequests."
                    );
	            return console.error(err);
	        }
	    }
	});
    };

    const acceptFriendrequest = (pendingId: number) => {
        /**
         * Accept a pending friendrequest
         */
        if (socketStateRef.current === SocketState.Disconnected) {
            setTimeout(() => acceptFriendrequest(pendingId), 500);
            return logger.error.magenta(
                "FriendsContext",
                "Currently disconnected from SocketServer. Retrying in 500ms."
            );
        }

        axios.patch(
            `${appContext.API_URL}/friendrequests/accept/${pendingId}`,
            {},
            { headers: { Authorization: `Bearer: ${userContext.token}` } }
        ).then((res: AxiosResponse<Response & { friend: Friend }>) => {
	    // .then((res: AxiosResponse<Response extends { frienditem: Friend }>) => {
	    const newFriend: FriendWithAvatar = {
	        ...res.data.friend,
	        avatar: `${appContext.API_URL}/users/get/avatar/${res.data.friend.friendId}?t=${new Date().getTime()}`
	    }
	    friendsContext.dispatchFriends({
                type: FriendsReducerActionType.ADD,
                payload: { friend: newFriend },
            });
	    dispatchPendings({
                type: PendingsReducerActionType.REMOVE,
                payload: { pendingId },
            });
	    notifContext.create(
                NotificationType.SUCCESS,
                res.data.message
            );
	    if (socketContext.socket) {
	        logger.log.magenta("FriendsContext", "Socket event emitted accept_friendrequest.");
	        socketContext.socket.emit("accept_friendrequest", pendingId, userContext.loggedInUserId, newFriend.friendId); 
	    }
	}).catch((err: AxiosError<Response>) => {
	    switch (err.message) {
	        case "Network Error": {
	            setTimeout(() => acceptFriendrequest(pendingId), 500);
	            return logger.error.magenta(
                        "FriendsContext",
                        "Network Error when accepting Friendrequest. Retrying in 500ms."
                    );
	        }
	        default: {
	            logger.error.magenta(
                        "FriendsContext", "Error when accepting Friendrequest."
                    );
	            if (err.response) return notifContext.create(
                        NotificationType.ERROR,
                        err.response.data.message
                    );
	            return console.error(err);
	        }
	    }
	});
    };

    const declineFriendrequest = (pendingId: number) => {
        /**
         * Decline a received pending friendrequest
         * or cancel a friendrequest you've send to a user
         */
        if (socketStateRef.current === SocketState.Disconnected) {
            setTimeout(() => declineFriendrequest(pendingId), 500);
            return logger.error.magenta(
                "FriendsContext",
                "Currently disconnected from SocketServer. Retrying in 500ms."
            );
        }

        axios.delete(
            `${appContext.API_URL}/friendrequests/decline/${pendingId}`,
            { headers: { Authorization: `Bearer: ${userContext.token}` } }
        ).then((res: AxiosResponse<Response>) => {
            const removedPending: Friendrequest = pendings.filter((pending: Friendrequest) => pending.id === pendingId)[0]!;
	    dispatchPendings({
                type: PendingsReducerActionType.REMOVE,
                payload: { pendingId },
            });
	    notifContext.create(NotificationType.INFO, res.data.message);

	    if (socketContext.socket) {
	        logger.log.magenta(
                    "FriendsContext",
                    "Socket event emitted decline_friendrequest."
                );
	        socketContext.socket.emit("decline_friendrequest", removedPending);
	    }
	}).catch((err: AxiosError<Response>) => {
	    switch (err.message) {
	        case "Network Error": {
	            setTimeout(() => declineFriendrequest(pendingId), 500);
	            return logger.error.magenta(
                        "FriendsContext",
                        "Network Error when declining Friendrequest. Retrying in 500ms."
                    );
	        }
	        default: {
	            logger.error.magenta(
                        "FriendsContext",
                        "Error when declining Friendrequest."
                    );
	            if (err.response) notifContext.create(
                        NotificationType.ERROR,
                        err.response.data.message
                    );
	            return console.error(err);
	        }
	    }
	});
    };

    const sendFriendrequest = (friendId: number) => {
        /**
         * Send friend request to friendId
         * returns true if successfull or an error string if unsuccessfullif unsuccessfull
         * @public
         * 
         * @param  {string}  friend
         */
        if (socketStateRef.current === SocketState.Disconnected) {
            setTimeout(() => sendFriendrequest(friendId), 500);
            return logger.error.magenta(
                "FriendsContext",
                "Currently disconnected from SocketServer. Retrying in 500ms."
            );
        }

        axios.post(
            `${appContext.API_URL}/friendrequests/send`,
            { fromUser: userContext.loggedInUserId, toUser: friendId },
            { headers: { Authorization: `Bearer: ${userContext.token}` } }
        ).then((res: AxiosResponse<Response & { friendrequest: Friendrequest }>) => {
	    dispatchPendings({
                type: PendingsReducerActionType.ADD,
                payload: { pending: res.data.friendrequest }
            });
	    notifContext.create(NotificationType.SUCCESS, `Friendrequest has been sent.`);

	    if (socketContext.socket) {
	        logger.log.magenta(
                    "FriendsContext",
                    "Socket event emitted send_friendrequest."
                );
	        socketContext.socket.emit(
                    "send_friendrequest",
                    res.data.friendrequest.from_user,
                    res.data.friendrequest.to_user
                );
	    }
	}).catch((err: AxiosError<Response>) => {
	    switch (err.message) {
	        case "Network Error": {
	            setTimeout(() => sendFriendrequest(friendId), 500);
	            return logger.error.magenta(
                        "FriendsContext",
                        "Network Error when sending Friendrequest. Retrying in 500ms."
                    );
	        }
	        default: {
	            logger.error.magenta(
                        "FriendsContext",
                        "Error when sending Friendrequest."
                    );
	            if (err.response) notifContext.create(
                        NotificationType.ERROR,
                        err.response.data.message
                    );
	            return console.error(err);
	        }
	    }
	});
    };

    /* ----------------------------------------------- Socket Event Listener Functions */
    const listenOnSendFriendrequest = (newFriendrequest: Friendrequest) => {
        dispatchPendings({ type: PendingsReducerActionType.ADD, payload: { pending: newFriendrequest } });
        const friendSendThisReq: boolean = newFriendrequest.from_user !== userContext.loggedInUserId;
        if (friendSendThisReq) return notifContext.create(NotificationType.INFO, `You got a new friendrequest.`);
    };

    const listenOnDeclineFriendrequest = (pendingId: number) => {
        dispatchPendings({ type: PendingsReducerActionType.REMOVE, payload: { pendingId } });
    };

    const listenOnAcceptFriendrequest = (pendingId: number, newFriend: Friend) => {
        dispatchPendings({ type: PendingsReducerActionType.REMOVE, payload: { pendingId } });
        friendsContext.dispatchFriends({ type: FriendsReducerActionType.ADD, payload: { friend: newFriend } });
    };

    useEffect(() => {
        /**
         * Socket EventListeners
         * "mounts" all event listerners for socket.io
         * @effect
         */
        if (socketContext.socket && userContext.loggedInUserId) {
            socketContext.socket.on(
	        "send_friendrequest",
	        (newFriendrequest: Friendrequest) => listenOnSendFriendrequest(newFriendrequest)
            );
            socketContext.socket.on(
	        "decline_friendrequest",
	        (pendingId: number) => listenOnDeclineFriendrequest(pendingId)
            );
            socketContext.socket.on(
	        "accept_friendrequest",
	        (pendingId: number, newFriend: Friend) => listenOnAcceptFriendrequest(pendingId, newFriend)
            );
        }

        return () => {
            if (socketContext.socket) {
	        socketContext.socket.off("send_friendrequest");
	        socketContext.socket.off("accept_friendrequest");
	        socketContext.socket.off("decline_friendrequest");
            }
        };
    }, [socketContext.socket, userContext.loggedInUserId]);

    return(
        <FriendrequestsContext.Provider value={{
            /* State */
            pendings, dispatchPendings,
            /* Methods */
            reset,
            fetchPendings,
            acceptFriendrequest,
            declineFriendrequest,
            sendFriendrequest,
        }}>
            { props.children }
        </FriendrequestsContext.Provider>
    );
}

export default FriendrequestsContextProvider;
