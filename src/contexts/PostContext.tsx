import React, { useState, useReducer, useEffect, useRef, createContext, useContext } from "react";
import axios, { AxiosResponse, AxiosError } from "axios";
import moment, { Moment } from "moment-timezone";
import logger from "../utils/logger";

import { PostContextValue,
	 UserContextValue,
	 FriendsContextValue,
	 AppContextValue,
	 SocketContextValue,
	 NotificationContextValue } from "../ts/types/contextvalue_types";
import { useUserContext } from "./UserContext";
import { useFriendsContext } from "./FriendsContext";
import { useAppContext } from "./AppContext";
import { useSocketContext } from "./SocketContext";
import { useNotificationContext } from "./NotificationContext";
import { NotificationType } from "../ts/types/notification_types";
import { Post, PostComment, PostsReducerActionType, PostsReducerAction } from "../ts/types/post_types";
import { Response } from "../ts/types/axios_types";
import { SocketState } from "../ts/types/socket_types";

export const PostContext = createContext<PostContextValue | undefined>(undefined);

export const usePostContext = () => {
    /**
     * Consume PostContext with error handling
     * @public
     *
     * returns {Context}    PostContext
     */
    const context = useContext(PostContext);
    if (context === undefined) {
        throw Error( "PostContext is undefined! Are you consuming PostContext outside of the PostContextProvider?" );
    }
    return context;
};

const reducerPosts: (state: Array<Post>, action: PostsReducerAction) => Array<Post> = (state, action) => {
    /**
     * reducer function for posts.
     * @private
     *
     * @param  {Post[]}  state
     * @param  {any}     action
     *
     * returns {Post[]}  state that has been alternated by the action
     */
    switch(action.type) {
        case PostsReducerActionType.FETCH: {
            logger.log.magenta("PostContext", "PostReducerActionType FETCH");
            if (!action.payload) return state;
            return action.payload.fetchedPosts;
        }

        case PostsReducerActionType.REMOVE: {
            logger.log.magenta("PostContext", "PostReducerActionType REMOVE");
            const updatedRemovePosts: Array<Post> = state.filter((post: Post) => post.id !== action.payload.postId);
            return updatedRemovePosts;
        }

        case PostsReducerActionType.ADD: {
            logger.log.magenta("PostContext", "PostReducerActionType ADD");
            const updatedAddPosts: Array<Post> = [...state, action.payload.newPost];
            return updatedAddPosts;
        }

        case PostsReducerActionType.LIKE_OR_DISLIKE: {
            logger.log.magenta("PostContext", "PostReducerActionType LIKE_OR_DISLIKE");
            const updatedLikeOrDislikePosts: Array<Post> = state.map((post: Post) => {
	        if (post.id === action.payload.postId) {
	            /* if the userId is already in the array remove it, if not append it to the array */
	            post.likes = post.likes.includes(action.payload.userId) ?
		                 post.likes.filter((likedUser: number) => likedUser !== action.payload.userId) :
		                 [...post.likes, action.payload.userId]; 
	        }
	        return post;
            });
            return updatedLikeOrDislikePosts;
        }

        case PostsReducerActionType.REMOVE_COMMENT: {
            logger.log.magenta("PostContext", "PostReducerActionType REMOVE_COMMENT");
            /* postId, commentId */
            const updatedRemoveCommentPosts: Array<Post> = state.map((post: Post) => {
	        if (post.id === action.payload.postId) {
	            post.comments = post.comments.filter((comment: PostComment) => comment.commentId !== action.payload.commentId);
	        }
	        return post;
            });
            return updatedRemoveCommentPosts;
        }

        case PostsReducerActionType.ADD_COMMENT: {
            logger.log.magenta("PostContext", "PostReducerActionType ADD_COMMENT");
            /* postId, newComment */
            const updatedAddCommentPosts: Array<Post> = state.map((post: Post) => {
	        if (post.id === action.payload.postId) {
	            post.comments = [...post.comments, action.payload.newComment];
	        }
	        return post;
            });
            return updatedAddCommentPosts;
        }

        case PostsReducerActionType.LOGOUT: {
            logger.log.magenta("PostContext", "PostReducerActionType LOGOUT");
            return [];
        }

        default: {
            logger.error.magenta("PostContext", "PostReducerActionType ${action.type}");
            console.error("Unknown Reducer Posts Error");
            return state;
        }
    }
};

interface Props {
    children: JSX.Element;
}

const PostContextProvider: React.FC<Props> = (props: Props) => {
    /**
     * The PostContext is for all storing functions & state that is essential for the whole App
     * but it isn't necessary to create a whole seperate Context for it
     * @component
     */
    const appContext: AppContextValue = useAppContext();
    const userContext: UserContextValue = useUserContext();
    const friendsContext: FriendsContextValue = useFriendsContext();
    const socketContext: SocketContextValue = useSocketContext();
    const notifContext: NotificationContextValue = useNotificationContext();

    const [posts, dispatchPosts] = useReducer(reducerPosts, []);

    /**
     * In React setTimoute will close all props/state
     * For example if you subscribe to an ID, and later want to unsubscribe, it would be a bug if ID could change over time
     * but in this case we want to have the current/latest/updated value of socketContext.state
     * to break out of the setTimeout loop in sendMessage/deleteGloballyMessage etc.
     * 
     * Source:
     * Cristian Salcescu
     * https://medium.com/programming-essentials/how-to-access-the-state-in-settimeout-inside-a-react-function-component-39a9f031c76f
     * 
     * https://github.com/facebook/react/issues/14010
     */
    const socketStateRef = useRef<SocketState>(socketContext.state);
    socketStateRef.current = socketContext.state;

    useEffect(() => {
        fetchPosts();
    }, [userContext.token, friendsContext.friends.length]);

    const [showCreatePost, setShowCreatePost] = useState<boolean>(false);
    const [newPostContent, setNewPostContent] = useState<string>("");
    const [newPostImages, setNewPostImages] = useState<Array<string>>([]);

    const reset = () => {
        /**
         * Reset the whole postcontext state
         * @public
         */
        dispatchPosts({ type: PostsReducerActionType.LOGOUT });
        setShowCreatePost(false);
        setNewPostContent("");
        setNewPostImages([]);
    };

    const fetchPosts = () => {
        /**
         * Fetch all posts
         * @public
         */
        if (!userContext.token) return logger.error.magenta("PostContext", "No Authentication Token was found.");

        axios.get(
            `${appContext.API_URL}/posts/`,
            { headers: { Authorization: `Bearer: ${userContext.token}` } }
        ).then((res: AxiosResponse<Array<Post>>) => {
	    dispatchPosts({ type: PostsReducerActionType.FETCH, payload: { fetchedPosts: res.data } });
	}).catch((err: AxiosError<Response>) => {
	    switch (err.message) {
	        case "Network Error": {
	            setTimeout(() => fetchPosts(), 1_000);
	            return logger.error.magenta("PostContext", "Network Error fetching messages. Retrying in 1000ms.");
	        }
	        default: {
	            logger.error.magenta("PostContext", "Error when fetching messages.");
                    return console.error(err);
	        }
	    }
	});
    };

    const handleLike = (postId: number, userId: number) => {
        /**
         * add or remove userId from the likes array
         * @public
         * 
         * @params  {number}  postId
         * @params  {number}  userId 
         */
        if (socketStateRef.current === SocketState.Disconnected) {
            setTimeout(() => handleLike(postId, userId), 500);
            return logger.error.magenta("PostContext", "Currently disconnected from SocketServer. Retrying in 500ms.");
        }

        axios.patch(
            `${appContext.API_URL}/posts/toggle-like/`,
            { postId },
            { headers: { Authorization: `Bearer: ${userContext.token}` } }
        ).then((_res: AxiosResponse<Response>) => {
	    dispatchPosts({ type: PostsReducerActionType.LIKE_OR_DISLIKE, payload: { postId, userId } });
	    socketContext.maybeReconnect();
	    if (socketContext.socket) {
	        logger.log.magenta("PostContext", "Socket event emitted like_or_dislike_post");
	        socketContext.socket.emit("like_or_dislike_post", userContext.loggedInUserId, postId);
	    }
	}).catch((err: AxiosError<Response>) => {
	    switch (err.message) {
	        case "Network Error": {
	            setTimeout(() => handleLike(postId, userId), 500);
	            return logger.error.magenta("PostContext", "Network Error posting like. Retrying in 500ms.");
	        }
	        default: {
	            logger.error.magenta("PostContext", "Error when posting like.");
	            return console.error(err);
	        }
	    }
	});
    };

    const handleDelete = (postId: number) => {
        /**
         * delete on of your own posts
         * @public
         * 
         * @params  {number}  postId
         */
        if (socketStateRef.current === SocketState.Disconnected) {
            setTimeout(() => handleDelete(postId), 500);
            return logger.error.magenta("PostContext", "Currently disconnected from SocketServer. Retrying in 500ms.");
        }

        axios.delete(
            `${appContext.API_URL}/posts/`,
            {
                data: { postId },
                headers: { Authorization: `Bearer ${userContext.token}` }
            }
        ).then((_res: AxiosResponse<Post>) => {
	         dispatchPosts({ type: PostsReducerActionType.REMOVE, payload: { postId } });
	         socketContext.maybeReconnect();
	         if (socketContext.socket) {
	             logger.log.magenta("PostContext", "Socket event emitted delete_post");
	             socketContext.socket.emit("delete_post", userContext.loggedInUserId, postId);
	         }
	     }).catch((err: AxiosError<Response>) => {
	         switch (err.message) {
	             case "Network Error": {
	                 setTimeout(() => handleDelete(postId), 500);
	                 return logger.error.magenta("PostContext", "Network Error when deleting post. Retrying in 500ms.");
	             }
	             default: {
	                 logger.error.magenta("PostContext", "Error when deleting post.");
	                 notifContext.create(NotificationType.ERROR, "Couldn't delete your post.");
	                 return console.error(err);
	             }
	         }
	     });
    };

    const handleDate: (writtenAt: number) => string = (writtenAt) => {
        /**
         * posted today show how many hours ago
         * if later than including yesterday show date of post
         * if les than 1h show now
         * @public
         * 
         * @params  {number}  writtenAt
         * 
         * @returns {string}  now | X hours ago | Date
         */
        const format: string = "MMM D";

        const post: Moment = moment(writtenAt);

        const passedMilisec: number = new Date().valueOf() - writtenAt;
        const passedHours: number = passedMilisec / 1000 / 60 / 60;
        const passedMinutes: number = passedHours * 60;

        if (passedMinutes < 3) return `now`;
        if (passedHours < 1) return `${Math.floor(passedMinutes)} minutes ago`;
        if (passedHours < 24) return `${Math.floor(passedHours)} hours ago`;
        return(post.tz(appContext.timezone).format(format));
    };

    /* ----------------------------------------------- Socket Event Listener Functions */
    const listenOnSendPost = (newPost: Post) => {
        dispatchPosts({ type: PostsReducerActionType.ADD, payload: { newPost } });
    };

    const listenOnDeletePost = (postId: number) => {
        dispatchPosts({ type: PostsReducerActionType.REMOVE, payload: { postId } });
    };

    const listenOnLikeOrDislikePost = (userId: number, postId: number) => {
        dispatchPosts({ type: PostsReducerActionType.LIKE_OR_DISLIKE, payload: { userId, postId } });
    };

    const listenOnCommentPost = (postId: number, newComment: PostComment) => {
        dispatchPosts({ type: PostsReducerActionType.ADD_COMMENT, payload: { postId, newComment } });
    };

    const listenOnRemoveCommentPost = (postId: number, commentId: number) => {
        dispatchPosts({ type: PostsReducerActionType.REMOVE_COMMENT, payload: { postId, commentId } });
    };

    useEffect(() => {
        /**
         * Socket EventListeners
         * "mounts" all event listerners for socket.io
         * @effect
         */
        if (socketContext.socket && userContext.loggedInUserId) {
            socketContext.socket.on(
	        "send_post",
	        (newPost: Post) => listenOnSendPost(newPost)
            );
            socketContext.socket.on(
	        "delete_post",
	        (postId: number) => listenOnDeletePost(postId)
            );
            socketContext.socket.on(
	        "like_or_dislike_post",
	        (userId: number, postId: number) => listenOnLikeOrDislikePost(userId, postId)
            );
            socketContext.socket.on(
	        "comment_post",
	        (postId: number, newComment: PostComment) => listenOnCommentPost(postId, newComment)
            );
            socketContext.socket.on(
	        "remove_comment_post",
	        (postId: number, commentId: number) => listenOnRemoveCommentPost(postId, commentId)
            );
        }

        return () => {
            if (socketContext.socket) {
	        socketContext.socket.off("send_post");
	        socketContext.socket.off("delete_post");
	        socketContext.socket.off("like_or_dislike_post");
	        socketContext.socket.off("comment_post");
	        socketContext.socket.off("remove_comment_post");
            }
        };
    }, [socketContext.socket, userContext.loggedInUserId] );


    return(
        <PostContext.Provider value={ {
            posts, dispatchPosts,
            showCreatePost, setShowCreatePost,
            newPostContent, setNewPostContent,
            newPostImages, setNewPostImages,
            reset,
            fetchPosts,
            handleLike,
            handleDelete,
            handleDate,
        } }>
            { props.children }
        </PostContext.Provider>
    );
};

export default PostContextProvider;
