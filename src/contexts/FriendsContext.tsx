import React, { useState, useReducer, useContext, useRef, createContext, useEffect } from "react";
import axios, { AxiosResponse, AxiosError } from "axios";
import logger from "../utils/logger";
import { useSocketContext } from "./SocketContext";
import { useUserContext } from "./UserContext";
import { useNotificationContext } from "./NotificationContext";
import { useAppContext } from "./AppContext";
import { FriendsContextValue,
	 SocketContextValue,
	 UserContextValue,
	 NotificationContextValue,
	 AppContextValue } from "../ts/types/contextvalue_types";
import { NotificationType } from "../ts/types/notification_types";
import { FriendsReducerActionType,
	 FriendsReducerAction,
	 PendingsReducerActionType,
	 PendingsReducerAction,
	 Friend,
	 FriendWithAvatar,
	 Friendrequest,
	 Status } from "../ts/types/friends_types";
import { Response } from "../ts/types/axios_types";
import { TinyInt } from "../ts/types/mariadb_types";
import { SocketState } from "../ts/types/socket_types";
import DefaultImage from "../assets/images/avatar.png";

const API_URL: string = (process.env.NODE_ENV === "production") ? `https://server.babbeln.app` : `http://localhost:3001`;

export const FriendsContext = createContext<FriendsContextValue | undefined>(undefined);

export const useFriendsContext = () => {
    /**
     * Consume FriendsContext with error handling
     * @public
     *
     * returns {Context}    FriendsContext
     */
    const context = useContext(FriendsContext);
    if (context === undefined) {
        throw Error( "FriendsContext is undefined! " +
                     "Are you consuming FriendsContext " +
                     "outside of the FriendsContextProvider? " );
    }
    return context;
};

const reducerFriends: (state: Array<FriendWithAvatar>, action: FriendsReducerAction) => Array<FriendWithAvatar> = (state, action) => {
    /**
     * reducer function for friends.
     * @private
     *
     * @param  {FriendsWithAvatarUrl[]}  state
     * @param  {any}                     action
     *
     * returns {FriendsWithAvatarUrl[]}  state that has been alternated by the action
     */
    switch (action.type) {
        case FriendsReducerActionType.FETCH: {
            logger.log.magenta(
                "FriendsContext",
                "FriendsReducerActionType FETCH"
            );
            if (!action.payload) return state;
            return action.payload.fetchedFriends as Array<FriendWithAvatar>;
        }

        case FriendsReducerActionType.REMOVE: {
            /** Removes a friend from your friendslist. */
            logger.log.magenta(
                "FriendsContext",
                "FriendsReducerActionType REMOVE"
            );
            const newState: Array<FriendWithAvatar> = state.filter((friend: FriendWithAvatar) => friend.friendId !== action.payload.friendId);
            return newState;
        }

        case FriendsReducerActionType.ADD: {
            /** add a friend to the friends state */
            logger.log.magenta(
                "FriendsContext",
                "FriendsReducerActionType ADD"
            );
            const isInState: boolean = state.some((friend: Friend) => friend.friendId === action.payload.friend.friendId);
            if (isInState) return state;
            const newState: Array<FriendWithAvatar> = [
                ...state,
                {
 	            ...action.payload.friend,
	            avatar: `${API_URL}/users/get/avatar/${action.payload.friend.friendId}?t=${new Date().getTime()}`,
                }
            ];
            return newState;
        }

        case FriendsReducerActionType.BLOCK: {
            /** Block a friend from your friendslist. */
            logger.log.magenta(
                "FriendsContext",
                "FriendsReducerActionType BLOCK"
            );
            const newState: Array<FriendWithAvatar> = state.map((friend: FriendWithAvatar) => {
	        if (friend.friendId === action.payload.friendId) friend.blocked = TinyInt.TRUE;
	        return friend;
            });
            return newState;
        }

        case FriendsReducerActionType.UNBLOCK: {
            /** Unblock a friend from your your friendslist. */
            logger.log.magenta(
                "FriendsContext",
                "FriendsReducerActionType UNBLOCK"
            );
            const newState: Array<FriendWithAvatar> = state.map((friend: FriendWithAvatar) => {
	        if (friend.friendId === action.payload.friendId) friend.blocked = TinyInt.FALSE;
	        return friend;
            });
            return newState;
        }

        case FriendsReducerActionType.UPDATE_USERNAME: {
            /** updates the username of a friend of yours */
            logger.log.magenta(
                "FriendsContext",
                "FriendsReducerActionType UPDATE_USERNAME"
            );
            const newState: Array<FriendWithAvatar> = state.map((friend: FriendWithAvatar) => {
	        if (friend.friendId === action.payload.friendId) {
	            friend.friendName = action.payload.username;
	        }
	        return friend;
            });
            return newState;
        }

        case FriendsReducerActionType.UPDATE_STATUS: {
            /** updates the online status of a friend of yours */
            logger.log.magenta(
                "FriendsContext",
                "FriendsReducerActionType UPDATE_STATUS"
            );
            const newState: Array<FriendWithAvatar> = state.map((friend: FriendWithAvatar) => {
	        if (friend.friendId === action.payload.friendId) {
	            friend.status = action.payload.status;
	        }
	        return friend;
            });
            return newState;
        }

        case FriendsReducerActionType.UPDATE_AVATAR: {
            /** update the url of the image of a friend */
            logger.log.magenta(
                "FriendsContext",
                "FriendsReducerActionType UPDATE_AVATAR"
            );
            const newState: Array<FriendWithAvatar> = state.map((friend: FriendWithAvatar) => {
	        if (friend.friendId === action.payload.friendId) {
	            friend.avatar = `${API_URL}/users/get/avatar/${friend.friendId}?t=${new Date().getTime()}`;
	        }
	        return friend;
            });
            return newState;
        }

        case FriendsReducerActionType.LOGOUT: {
            logger.log.magenta(
                "FriendsContext",
                "FriendsReducerActionType LOGOUT"
            );
            return [];
        }

        default: {
            logger.error.magenta(
                "FriendsContext",
                `FriendsReducerActionType ${action.type}`
            );
            return state;
        }
    }
};

interface Props {
    children: JSX.Element;
}

const FriendsContextProvider: React.FC<Props> = (props: Props) => {

    const socketContext: SocketContextValue = useSocketContext();
    const userContext: UserContextValue = useUserContext();
    const notifContext: NotificationContextValue = useNotificationContext();
    const appContext: AppContextValue = useAppContext();

    const [friends, dispatchFriends] = useReducer(reducerFriends, []);

    /**
     * In React setTimoute will close all props/state
     * For example if you subscribe to an ID, and later want to unsubscribe, it would be a bug if ID could change over time
     * but in this case we want to have the current/latest/updated value of socketContext.state
     * to break out of the setTimeout loop in sendMessage/deleteGloballyMessage etc.
     * 
     * Source:
     * Cristian Salcescu
     * https://medium.com/programming-essentials/how-to-access-the-state-in-settimeout-inside-a-react-function-component-39a9f031c76f
     * 
     * https://github.com/facebook/react/issues/14010
     */
    const socketStateRef = useRef<SocketState>(socketContext.state)
    socketStateRef.current = socketContext.state;

    useEffect(() => {
        /**
         * Download all friends and pending friendrequests
         * @effect
         */
        fetchFriends();
    }, [userContext.loggedInUserId]);

    const [removeFriendId, setRemoveFriendId] = useState<number>(0);
    /* (Global-)State for Friends Component TODO: move this to Friends.tsx */
    const [showOnline, setShowOnline] = useState<boolean>(true);
    const [showAll, setShowAll] = useState<boolean>(false);
    const [showPending, setShowPending] = useState<boolean>(false);
    const [showBlocked, setShowBlocked] = useState<boolean>(false);
    const [showAddFriend, setShowAddFriend] = useState<boolean>(false);


    const reset = () => {
        /**
         * Resets the FriendsContext State 
         * example: when logging out
         * @public
         */
        dispatchFriends({ type: FriendsReducerActionType.LOGOUT });
        setRemoveFriendId(0);
        setShowOnline(false);
        setShowAll(false);
        setShowPending(false);
        setShowBlocked(false);
        setShowAddFriend(false);
    };

    const fetchFriends = () => {
        /**
         * Fetch all of your friends from the DB
         * & set it as the new friends state
         * @public
         */
        if (!userContext.token) return logger.error.magenta(
            "FriendsContext",
            "Tried to fetch friends, but no authentication token is available"
        );

        axios.get(
            `${appContext.API_URL}/friends/get`,
            { headers: { Authorization: `Bearer: ${userContext.token}` } }
        ).then((res: AxiosResponse<Array<Friend>>) => {
	    const fetchedFriends: Array<Friend> = res.data;
	    const friendsWithAvatarUrl: Array<FriendWithAvatar> = fetchedFriends.map((friend: Friend) => {
	        const friendWithAvatarUrl = {
	            ...friend,
	            avatar: `${appContext.API_URL}/users/get/avatar/${friend.friendId}?t=${new Date().getTime()}`,
	        };
	        return friendWithAvatarUrl;
	    });
	    dispatchFriends({ type: FriendsReducerActionType.FETCH, payload: { fetchedFriends: friendsWithAvatarUrl } });
	}).catch((err: AxiosError<Response>) => {
	    switch (err.message) {
	        case "Network Error": {
	            setTimeout(() => fetchFriends(), 1_000);
	            return logger.error.magenta(
                        "FriendsContext",
                        "Network Error when fetching Friends. Retrying in 1000ms."
                    );
	        }
	        default: {
	            logger.error.magenta(
                        "FriendsContext",
                        "Error when fetching Friends."
                    );
	            return console.error(err);
	        }
	    }
	});
    };

    const removeFriend = (friendId: number) => {
        if (!userContext.token) return logger.error.magenta(
            "FriendsContext",
            "Tried to remove a friend, but no authentication token is available"
        );

        if (socketStateRef.current === SocketState.Disconnected) {
            setTimeout(() => removeFriend(friendId), 500);
            return logger.error.magenta(
                "FriendsContext",
                "Currently disconnected from SocketServer. Retrying in 500ms"
            );
        }

        axios.delete(
            `${appContext.API_URL}/friends/remove`,
            {
                data: { friendId },
                headers: { Authorization: `Bearer: ${userContext.token}` },
            },
        ).then((_res: AxiosResponse<Response>) => {
	    dispatchFriends({ type: FriendsReducerActionType.REMOVE, payload: { friendId } });

	    socketContext.maybeReconnect();
	    if (socketContext.socket) {
	        logger.log.magenta(
                    "FriendsContext",
                    "Socket event emitted remove_friend."
                );
	        socketContext.socket.emit(
                    "remove_friend",
                    userContext.loggedInUserId,
                    friendId
                );
	    }
	}).catch((err: AxiosError<Response>) => {
	    switch (err.message) {
	        case "Network Error": {
	            setTimeout(() => removeFriend(friendId), 500);
	            return logger.error.magenta(
                        "FriendsContext",
                        "Network Error when removing Friend. Retrying in 500ms."
                    );
	        }
	        default: {
	            logger.error.magenta(
                        "FriendsContext",
                        "Error when removing Friend."
                    );
	            if (err.response) notifContext.create(
                        NotificationType.ERROR,
                        err.response.data.message
                    );
	            return console.error(err);
	        }
	    }
        });
    };

    const blockFriend = (friendId: number) => {
        if (!userContext.token) return logger.error.magenta(
            "FriendsContext",
            "Tried to block a friend, but no authentication token is available"
        );

        if (socketStateRef.current === SocketState.Disconnected) {
            setTimeout(() => blockFriend(friendId), 500);
            return logger.error.magenta(
                "FriendsContext",
                "Currently disconnected from SocketServer. Retrying in 500ms"
            );
        }

        axios.patch(
            `${appContext.API_URL}/friends/block`,
            { friendId },
            { headers: { Authorization: `Bearer: ${userContext.token}` } }
        ).then((_res: AxiosResponse<Response>) => {
	    dispatchFriends({
                type: FriendsReducerActionType.BLOCK,
                payload: { friendId }
            });
        }).catch((err: AxiosError<Response>) => {
	    switch (err.message) {
	        case "Network Error": {
	            setTimeout(() => blockFriend(friendId), 500);
	            return logger.error.magenta(
                        "FriendsContext",
                        "Network Error when blocking Friend. Retrying in 500ms."
                    );
	        }
	        default: {
	            logger.error.magenta(
                        "FriendsContext",
                        "Error when blocking Friend."
                    );
	            if (err.response) notifContext.create(
                        NotificationType.ERROR,
                        err.response.data.message
                    );
	            return console.error(err);
	        }
	    }
        });
    };

    const unblockFriend = (friendId: number) => {
        if (!userContext.token) return logger.error.magenta(
            "FriendsContext",
            "Tried to unblock a friend, but no authentication token is available"
        );

        if (socketStateRef.current === SocketState.Disconnected) {
            setTimeout(() => unblockFriend(friendId), 500);
            return logger.error.magenta(
                "FriendsContext",
                "Currently disconnected from SocketServer. Retrying in 500ms"
            );
        }

        axios.patch(
            `${appContext.API_URL}/friends/unblock`,
            { friendId },
            { headers: { Authorization: `Bearer: ${userContext.token}` } }
        ).then((_res: AxiosResponse<Response>) => {
	    dispatchFriends({ type: FriendsReducerActionType.UNBLOCK, payload: { friendId } });
        }).catch((err: AxiosError<Response>) => {
	    switch (err.message) {
	        case "Network Error": {
	            setTimeout(() => unblockFriend(friendId), 500);
	            return logger.error.magenta(
                        "FriendsContext",
                        "Network Error when unblocking Friend. Retrying in 500ms."
                    );
	        }
	        default: {
	            logger.error.magenta(
                        "FriendsContext",
                        "Error when unblocking Friend."
                    );
	            if (err.response) notifContext.create(
                        NotificationType.ERROR,
                        err.response.data.message
                    );
	            return console.error(err);
	        }
	    }
        });
    };

    const getFriend = (friendId: number): FriendWithAvatar | undefined => {
        /**
         * Returns single frienditem by id from the state
         * @public
         * 
         * @params   {number}              friendId
         * 
         * @returns  {Friend | undefined}  frienditem
         */
        return friends.filter(
            (friend: FriendWithAvatar | undefined) => {
                if (friend) return friend.friendId === friendId;
                return false;
            }
        )[0];
    };

    const getAvatar = (friend: FriendWithAvatar | undefined): string => {
        /**
         * Return the correct url of the profile picture of a friend
         * if he/she is still the clients friend & isn't blocked
         * @public
         * 
         * @returns  {DefaultImage | string}  picSrc
         */
        if (!friend) return DefaultImage; /* defaultimage is of type string */
        if (friend.blocked === TinyInt.TRUE) return DefaultImage;
        return friend.avatar;
    };

    /* ----------------------------------------------- Socket Event Listener Functions */
    const listenOnRemoveFriend = (friendId: number) => {
        dispatchFriends({ type: FriendsReducerActionType.REMOVE, payload: { friendId } });
    };

    const listenOnUpdateStatus = (friendId: number, status: Status) => {
        dispatchFriends({ type: FriendsReducerActionType.UPDATE_STATUS, payload: { friendId, status } });
    };

    const listenOnUpdateUsername = (friendId: number, username: string) => {
        dispatchFriends({ type: FriendsReducerActionType.UPDATE_USERNAME, payload: { friendId, username } });
    };

    const listenOnUpdateAvatar = (friendId: number) => {
        dispatchFriends({ type: FriendsReducerActionType.UPDATE_AVATAR, payload: { friendId } });
    };

    useEffect(() => {
        /**
         * Socket EventListeners
         * "mounts" all event listerners for socket.io
         * @effect
         */
        if (socketContext.socket && userContext.loggedInUserId) {
            socketContext.socket.on(
	        "remove_friend",
	        (friendId: number) => listenOnRemoveFriend(friendId)
            );
            socketContext.socket.on(
	        "update_status",
	        (friendId: number, status: Status) => listenOnUpdateStatus(friendId, status)
            );
            socketContext.socket.on(
	        "update_username",
	        (friendId: number, username: string) => listenOnUpdateUsername(friendId, username)
            );
            socketContext.socket.on(
	        "update_avatar",
	        (friendId: number) =>  listenOnUpdateAvatar(friendId)
            );
        }

        return () => {
            if (socketContext.socket) {
	        socketContext.socket.off("remove_friend");
	        socketContext.socket.off("update_status");
	        socketContext.socket.off("update_username");
	        socketContext.socket.off("update_avatar");
            }
        };
    }, [socketContext.socket, userContext.loggedInUserId]);

    return(
        <FriendsContext.Provider value={{
            /* State */
            friends, dispatchFriends,
            removeFriendId, setRemoveFriendId,
            showOnline, setShowOnline,
            showAll, setShowAll,
            showPending, setShowPending,
            showBlocked, setShowBlocked,
            showAddFriend, setShowAddFriend,
            /* Methods */
            reset,
            fetchFriends,
            removeFriend,
            blockFriend,
            unblockFriend,
            getFriend,
            getAvatar,
        }}>
            { props.children }
        </FriendsContext.Provider>
    );
}

export default FriendsContextProvider;
