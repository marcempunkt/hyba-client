import React, { useState, useContext, createContext } from "react";
import { NotificationContextValue } from "../ts/types/contextvalue_types";
import { NotificationType } from "../ts/types/notification_types";

// Cannot find module '*.mp3' or its corresponding type declarations.
import NotificationSound from  "../assets/sounds/notification.mp3";
import ReceivedMessageSound from  "../assets/sounds/new_message.mp3";
import SendMessageSound from  "../assets/sounds/send_message.mp3";
import StartCallSound from  "../assets/sounds/start_call.mp3";
import EndCallSound from  "../assets/sounds/end_call.mp3";

export const NotificationContext = createContext<NotificationContextValue | undefined>(undefined);

export const useNotificationContext = () => {
  /**
   * Consume NotificationContext with error handling
   * @public
   *
   * returns {Context}    NotificationContext
   */
  const context = useContext(NotificationContext);
  if (context === undefined) {
    throw Error( "NotificationContext is undefined! Are you consuming NotificationContext outside of the NotificationContxetProvider?" );
  }
  return context;
}

interface Props {
  children: JSX.Element;
}

const NotificationContextProvider: React.FC<Props> = (props: Props) => {

  const [message, setMessage] = useState<string>("");
  const [notificationType, setNotificationType] = useState<NotificationType>(NotificationType.INFO);
  const [showMessage, setShowMessage] = useState<boolean>(false);

  let timeoutID: ReturnType<typeof setTimeout>;

  const timeout = {
    /**
     * Stores create and cancel functions, so it can cancel a specific notification,
     * othwerise the timeoutID will be in the nether and cannot be accessed.
     * Is only used in Notification.tsx for spawning a Notification elsewhere please use the create function!
     * @public
     */
    create: () => {
      // if (typeof this.timeoutID = 'number') return this.cancel();
      timeoutID = setTimeout( () => {
        // in case it gets closed before
        if (showMessage) return setShowMessage(false);
      }, 5000);
    },
    cancel: () => clearTimeout(timeoutID),
  };

  const create = (notifType: NotificationType, msg: string) => {
    /**
     * TODO make notifications an array with each having their own timer & make it look prettier
     * Spawns a notification.
     * @public
     *
     * @param  {NotificationType}  notifType
     * @param  {string}            msg        content of the notification
     */
    // clearTimeout();
    setMessage(msg);
    setNotificationType(notifType);
    timeout.cancel();
    setShowMessage(true);
  };

  const playSound = (jingle: string) => {
    const sound: HTMLAudioElement = new Audio(jingle);
    sound.loop = false;
    sound.volume = 0.5;
    sound.load();
    sound.play().catch((err: unknown) => console.error(`${err}`));
  };

  const playSoundReceivedMessage  = () => playSound(NotificationSound);
  const playSoundSendMessage      = () => playSound(SendMessageSound);
  const playSoundFriendrequest    = () => playSound(NotificationSound);
  const playSoundStartCall        = () => playSound(StartCallSound);
  const playSoundEndCall          = () => playSound(EndCallSound);
  const playSoundStartScreenshare = () => playSound(StartCallSound);
  const playSoundCloseScreenshare = () => playSound(EndCallSound);

  return(
    <NotificationContext.Provider value={
    {
      message, setMessage,
      notificationType, setNotificationType,
      showMessage, setShowMessage,
      timeout,
      create,
      playSoundReceivedMessage,
      playSoundSendMessage,
      playSoundFriendrequest,
      playSoundStartCall,
      playSoundEndCall,
      playSoundStartScreenshare,
      playSoundCloseScreenshare,
    }
    }>
      {props.children}
    </NotificationContext.Provider>
  );
}

export default NotificationContextProvider;
