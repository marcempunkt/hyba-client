import React, { useEffect, useState, useReducer, useRef, useContext, createContext } from "react";
import axios, { AxiosResponse, AxiosError } from "axios";
import logger from "../utils/logger";

import { useNotificationContext } from "./NotificationContext";
import { useSocketContext } from "./SocketContext";
import { useUserContext } from "./UserContext";
import { useAppContext } from "./AppContext";
import { MessageContextValue,
	 UserContextValue,
	 SocketContextValue,
	 NotificationContextValue,
	 AppContextValue } from "../ts/types/contextvalue_types";
import { MessageState,
	 ReducerActionType,
	 ReducerAction,
	 ClientMessage,
	 ServerMessage,
	 SendMessage,
	 Draft } from "../ts/types/message_types";
import { SocketState } from "../ts/types/socket_types";
import { TinyInt } from "../ts/types/mariadb_types";
import { NotificationType } from "../ts/types/notification_types";
import { Response } from "../ts/types/axios_types";

function reducerMessages(state: MessageState, action: ReducerAction): MessageState {
  /**
   * reducer function for messages.
   * @private
   *
   * @param  {MessageState}  Array<ClientMessage>
   * @param  {any}           action         action object for reducer function
   *
   * returns {MessageState}  state that has been altered by the action
   */
  switch (action.type) {
    case ReducerActionType.FETCH: {
      logger.log.magenta("MessageContext", "MessageReducerActionType FETCH");
      return action.payload
    }

    case ReducerActionType.ADD_MESSAGE: {
      logger.log.magenta("MessageContext", "MessageReducerActionType ADD_MESSAGE");
      return [...state, action.payload];
    }

    case ReducerActionType.DELETE_MESSAGE: {
      logger.log.magenta("MessageContext", "MessageReducerActionType DELETE_MESSAGE");
      const newState: Array<ClientMessage> = state.map((msg: ClientMessage) => {
        if (action.payload.id === msg.id) { msg.is_deleted = TinyInt.TRUE; }
        return msg;
      });
      return newState;
    }

    case ReducerActionType.DELETE_CHAT: {
      logger.log.magenta("MessageContext", "MessageReducerActionType DELETE_CHAT");
      const newState: Array<ClientMessage> = state.filter((msg: ClientMessage) => {
	const iSent:      boolean = msg.from_user === action.payload.fromUserId && msg.to_user === action.payload.toDeleteUserId;
	const friendSent: boolean = msg.to_user === action.payload.fromUserId && msg.from_user === action.payload.toDeleteUserId;
	if (iSent || friendSent) {
	  return false;
	} else {
	  return true;
	}
      });
      return newState;
    }

    case ReducerActionType.LOGOUT: {
      logger.log.magenta("MessageContext", "MessageReducerActionType LOGOUT");
      return [];
    }

    default: {
      logger.error.magenta("MessageContext", "MessageReducerActionType ${action.type}");
      console.error("Unknown Reducer Message Error");
      return state;
    }
  }
}

export const MessageContext = createContext<MessageContextValue | undefined>(undefined);

export const useMessageContext = () => {
  /**
   * Consume SocketContext with error handling
   * @public
   *
   * returns {Context}    SocketContext
   */
  const context = useContext(MessageContext);
  if (context === undefined) {
    throw Error( "MessageContext is undefined! Are you consuming MessageContext outside of the MessageContextProvider?" );
  }
  return context;
};

interface Props {
  children: JSX.Element;
}

const MessageContextProvider: React.FC<Props> = (props: Props) => {

  const socketContext: SocketContextValue = useSocketContext();
  const userContext: UserContextValue = useUserContext();
  const notifContext: NotificationContextValue = useNotificationContext();
  const appContext: AppContextValue = useAppContext();

  const [messages, dispatchMessages] = useReducer(reducerMessages, []);
  const [showMessagesFromUserId, setShowMessagesFromUserId] = useState<number>(0);
  const [drafts, setDrafts] = useState<Array<Draft>>([]);
  const [showSearchMessages, setShowSearchMessages] = useState<boolean>(false);

  /**
   * In React setTimoute will close all props/state
   * For example if you subscribe to an ID, and later want to unsubscribe, it would be a bug if ID could change over time
   * but in this case we want to have the current/latest/updated value of socketContext.state
   * to break out of the setTimeout loop in sendMessage/deleteGloballyMessage etc.
   * 
   * Source:
   * Cristian Salcescu
   * https://medium.com/programming-essentials/how-to-access-the-state-in-settimeout-inside-a-react-function-component-39a9f031c76f
   * 
   * https://github.com/facebook/react/issues/14010
   */
  const socketStateRef = useRef<SocketState>(socketContext.state);
  socketStateRef.current = socketContext.state;

  useEffect(() => { fetchMessages(); }, [userContext.loggedInUserId]);

  /* ----------------------------------------------------------------------------------------- ~Functions~ */
  const reset = () => {
    /**
     * Cleanup the SocketContext State to reset it for the next User to login
     * @public 
     */
    dispatchMessages({ type: ReducerActionType.LOGOUT });
    setShowMessagesFromUserId(0);
    setDrafts([]);
  };

  const fetchMessages = () => {
    /**
     * Fetch all messages associated with the user
     * @public
     */
    if (!userContext.token) return logger.error.magenta("MessageContext", "No Authentication Token was found.");

    /* Get Messages of currently logged in User */
    axios.get(`${appContext.API_URL}/messages/`, { headers: { Authorization: `Bearer: ${userContext.token}` } })
	 .then((res: AxiosResponse<Array<ServerMessage>>) => {
	   const newMessages: Array<ClientMessage> = res.data.map((msg: ServerMessage) => ({ ...msg, is_deleted: TinyInt.FALSE }));
	   dispatchMessages({ type: ReducerActionType.FETCH, payload: newMessages });
	 }).catch((err: AxiosError<Response>) => {
	   switch (err.message) {
	     case "Network Error": {
	       setTimeout(() => fetchMessages(), 1_000);
	       return logger.error.magenta("MessageContext", "Network Error fetching messages. Retrying in 1000ms.");
	     }
	     default: {
	       logger.error.magenta("MessageContext", "Error when fetching messages.");
               return console.error(err);
	     }
	   }
	 });
  };

  const getSingleMessage: (msgId: number) => ClientMessage | null = (msgId) => {
    const message: Array<ClientMessage> = messages.filter((msg: ClientMessage) => msg.id === msgId);
    if (message.length) return message[0];
    return null;
  };

  /* ----------------------------------------------------------------------------------------- ~Socket.io Event Listeners~ */
  const sendMessage = (msg: SendMessage) => {
    /**
     * Send Message
     * if it failes go into a recursive-loop and retry every 0.5s
     * @public
     */
    if (socketStateRef.current === SocketState.Disconnected) {
      setTimeout(() => sendMessage(msg), 500);
      return logger.error.magenta("MessageContext", "Currently disconnected from SocketServer. Retrying in 500ms.");
    } 

    axios.post(`${appContext.API_URL}/messages/send/`, { msg }, { headers: { Authorization: `Bearer: ${userContext.token}` } })
	 .then((res: AxiosResponse<Array<ServerMessage | undefined>>) => {
           res.data.map((msg: ServerMessage | undefined) => {
	     if (!msg) return;
	     if (msg.owner === userContext.loggedInUserId) {
	       const newMessage: ClientMessage = {
		 ...msg,
		 is_deleted: TinyInt.FALSE,
	       };
	       dispatchMessages({ type: ReducerActionType.ADD_MESSAGE, payload: newMessage});
	     }

	     socketContext.maybeReconnect();
	     if (socketContext.socket) {
	       logger.log.magenta("MessageContext", "Socket event emitted send_msg");
	       socketContext.socket.emit("send_msg", msg);
	     }
	   });
	 })
	 .catch((err: AxiosError<Response>) => {
	   switch (err.message) {
	     case "Network Error": {
	       setTimeout(() => sendMessage(msg), 500);
	       return logger.error.magenta("MessageContext", "Network Error sending a message. Retrying in 500ms.");
	     }
	     default: {
	       console.error(err);
	       if (err.response) { notifContext.create(NotificationType.ERROR, err.response.data.message); }
	     }
	   }
	 });
  };

  const listenSendMessage = (serverMsg: ServerMessage) => {
    /**
     * Listen to socket event "send_msg"
     * 1. create a message object that the client can understand
     * 2. dispatch the new message to the state
     * 3. play message notification sound
     */
    const newMessage: ClientMessage = {
      ...serverMsg,
      is_deleted: TinyInt.FALSE,
    };

    if (newMessage.to_user === userContext.loggedInUserId) {
      notifContext.playSoundReceivedMessage();
    }

    return dispatchMessages({ type: ReducerActionType.ADD_MESSAGE, payload: newMessage });
  };

  const deleteMessageGlobally: (msgId: number) => void = (msgId) => {
    /**
     * delete a message for all users
     * @public
     *
     * @param   {number}  msgId: id of the message that will be deleted
     *
     * @returns {boolean} success|fail
     */
    if (!userContext.token) return logger.error.magenta("MessageContext", "No Authentication Token was found.");

    if (socketStateRef.current === SocketState.Disconnected) {
      setTimeout(() => deleteMessageGlobally(msgId), 500);
      return logger.error.magenta("MessageContext", "Currently disconnected from SocketServer. Retrying in 500ms.");
    }

    /* patch to delete message from database */
    axios
      .delete(`${appContext.API_URL}/messages/delete/globally/${msgId}`, { headers: { Authorization: `Bearer: ${userContext.token}` } })
      .then((res: AxiosResponse<Response & { deletedMessages: Array<ServerMessage> }>) => {
	/* All messages from the DB has been deleted */
	/* Now delete all of them on the client side */
	res.data.deletedMessages.map(
	  (deletedMsg: ServerMessage) => dispatchMessages({ type: ReducerActionType.DELETE_MESSAGE, payload: { id: deletedMsg.id } })
	);
	/* Emit event to all other users */
	socketContext.maybeReconnect();
	if (socketContext.socket) {
	  logger.log.magenta("MessageContext", "Socket event emitted delete_msg");
	  socketContext.socket.emit("delete_msg", res.data.deletedMessages);
	}
      })
      .catch((err: AxiosError<Response>) => {
	switch (err.message) {
	  case "Network Error": {
	    setTimeout(() => deleteMessageGlobally(msgId), 500);
	    return logger.error.magenta("MessageContext", "Network Error deleting a message globally. Retrying in 500ms.");
	  }
	  default: {
	    console.error(err);
	    if (err.response) {
	      notifContext.create(NotificationType.ERROR, err.response.data.message);
	    }
	  }
	}
      });
  };

  const deleteMessageOnlyForMe: (msgId: number) => void = (msgId) => {
    /**
     * delete a message only for the currently logged in user
     * @public
     *
     * @param  {number}  msgId: id of the message that will be deleted
     */
    if (!userContext.token) return logger.error.magenta("MessageContext", "No Authentication Token was found.");

    if (socketStateRef.current === SocketState.Disconnected) {
      setTimeout(() => deleteMessageOnlyForMe(msgId), 500);
      return logger.error.magenta("MessageContext", "Currently disconnected from SocketServer. Retrying in 500ms.");
    }

    /* path to delete message from database */
    axios
      .delete(`${appContext.API_URL}/messages/delete/privately/${msgId}`, { headers: { Authorization: `Bearer: ${userContext.token}` } })
      .then((_res: AxiosResponse<Response>) => {
        /* delete message for client */
        dispatchMessages({ type: ReducerActionType.DELETE_MESSAGE, payload: { id: msgId } });
      })
      .catch((err: AxiosError<Response>) => {
	switch (err.message) {
	  case "Network Error": {
	    setTimeout(() => deleteMessageOnlyForMe(msgId), 500);
	    return logger.error.magenta("MessageContext", "Network Error deleting a message only for client. Retrying in 500ms.");
	  }
	  default: {
	    console.error(err);
	    if (err.response) {
	      notifContext.create(NotificationType.ERROR, err.response.data.message);
	    }
	  }
	}
      });
  };

  const deleteChat = (deleteChatWithUser: number) => {
    /**
     * Delete a whole chat
     * @public
     * 
     * @param  {number}  deleteChatWithUser
     */
    if (!userContext.loggedInUserId || !deleteChatWithUser) {
      return logger.error.magenta("MessageContext", "Couldn't delete chat. Reason: No loggedInUserId or deleteChatWithUser.");
    }

    if (!userContext.token) return logger.error.magenta("MessageContext", "No Authentication Token found");

    if (socketStateRef.current === SocketState.Disconnected) {
      setTimeout(() => deleteChat(deleteChatWithUser), 500);
      return logger.error.magenta("MessageContext", "Currently disconnected from SocketServer. Retrying in 500ms.");
    } 

    const body = {
      fromUserId: userContext.loggedInUserId,
      toDeleteUserId: deleteChatWithUser,
      token: userContext.token,
    };

    axios
      .patch(`${appContext.API_URL}/messages/delete/chat`, body, { headers: { Authorization: `Bearer: ${userContext.token}` } })
      .then((res: AxiosResponse<Response>) => {
	/* remove chat from state */
	dispatchMessages({
	  type: ReducerActionType.DELETE_CHAT,
	  payload: {
	    fromUserId: userContext.loggedInUserId,
	    toDeleteUserId: deleteChatWithUser,
	  },
	});
	/* emit socket event */
	socketContext.maybeReconnect();
	if (socketContext.socket) {
	  logger.log.magenta("MessageContext", "Socket event emitted delete_chat.");
	  socketContext.socket.emit("delete_chat", userContext.loggedInUserId, deleteChatWithUser);
	}
	notifContext.create(NotificationType.SUCCESS, res.data.message);
	setShowMessagesFromUserId(0);
      })
      .catch((err: AxiosError<Response>) => {
	switch (err.message) {
	  case "Network Error": {
	    setTimeout(() => deleteChat(deleteChatWithUser), 500);
	    return logger.error.magenta("MessageContext", "Network Error deleting a chat. Retrying in 500ms.");
	  }
	  default: {
	    console.error(err);
	    if (err.response) {
	      notifContext.create(NotificationType.ERROR, err.response.data.message);
	    }
	  }
	}
      });
  };

  const listenDeleteMessage = (deletedMessages: Array<ServerMessage>) => {
    /* Remeber: It could be more than one person that we would have to send the delete_msg event
     * For Example in a group chat
     */
    deletedMessages.forEach((deletedMsg: ServerMessage) => {
      dispatchMessages({ type: ReducerActionType.DELETE_MESSAGE, payload: { id: deletedMsg.id } });
    });
  };

  const listenDeleteChat = (toDeleteChatUserId: number) => {
    /**
     * Delete the whole chat with a user
     */
    dispatchMessages({ type: ReducerActionType.DELETE_CHAT, payload: { fromUserId: userContext.loggedInUserId, toDeleteChatUserId } });
  };

  useEffect(() => {
    /**
     * Socket EventListeners
     * "mounts" all event listerners for socket.io
     * @effect
     */
    if (socketContext.socket && userContext.loggedInUserId) {
      socketContext.socket.on("send_msg", (msg: ServerMessage) => listenSendMessage(msg));
      socketContext.socket.on("delete_msg", (deletedMessages: Array<ServerMessage>) => listenDeleteMessage(deletedMessages));
      socketContext.socket.on("delete_chat", (deleteChatUserId: number) => listenDeleteChat(deleteChatUserId));
    }
    return () => {
      if (socketContext.socket) {
	socketContext.socket.off("send_msg");
	socketContext.socket.off("delete_msg");
	socketContext.socket.off("delete_chat");
      }
    };
  }, [socketContext.socket, userContext.loggedInUserId]);

  return (
    <MessageContext.Provider
      value={{
	/* ~State~ */
	messages, dispatchMessages,
	showMessagesFromUserId, setShowMessagesFromUserId,
	drafts, setDrafts,
	showSearchMessages, setShowSearchMessages,
	/* ~Methods~ */
	reset,
	fetchMessages,
	sendMessage,
	deleteMessageGlobally,
	deleteMessageOnlyForMe,
	deleteChat,
	getSingleMessage,
      }}>
      {props.children}
    </MessageContext.Provider>
  );

};

export default MessageContextProvider;
