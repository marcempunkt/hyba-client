import React, { useEffect, MouseEvent } from "react";
import "./sass/styles.scss";
import { BrowserRouter, HashRouter, Routes, Route } from "react-router-dom";

// Components
import Notification from "./components/Notification";
import Babbeln from "./components/Babbeln";
import Start from "./components/Start";

// Contexts
import AppContextProvider from "./contexts/AppContext";
import SocketContextProvider from "./contexts/SocketContext";
import NotificationContextProvider from "./contexts/NotificationContext";
import FriendsContextProvider from "./contexts/FriendsContext";
import FriendrequestsContextProvider from "./contexts/FriendrequestsContext";
import ModalContextProvider from "./contexts/ModalContext";
import ContextmenuContextProvider from "./contexts/ContextmenuContext";
import PostContextProvider from "./contexts/PostContext";
import UserContextProvider from "./contexts/UserContext";
import CallContextProvider from "./contexts/CallContext";
import MessageContextProvider from "./contexts/MessageContext";
import BlogContextProvider from "./contexts/BlogContext";

const App: React.FC = () => {

    const preventContextmenu = (e: MouseEvent<HTMLDivElement>) => e.preventDefault();

    return (
        <main onContextMenu={ preventContextmenu }> 
            <AppContextProvider>
	        <NotificationContextProvider>
                    <SocketContextProvider>
	                <UserContextProvider>
	                    <MessageContextProvider>
		                <FriendsContextProvider>
		                    <FriendrequestsContextProvider>
		                        <PostContextProvider>
		                            <BlogContextProvider>
		                                <ModalContextProvider>
			                            <CallContextProvider>
			                                <ContextmenuContextProvider>
			                                    <Notification />
			                                    {
			                                        (process.env.BUILD_TARGET === "electron") ?
			                                        <HashRouter>
				                                    <Routes>
				                                        <Route path="/" element={ <Babbeln /> } />
				                                        <Route path="/getting-started" element={ <Start /> } />
				                                    </Routes>
			                                        </HashRouter>
			                                        :
			                                        <BrowserRouter>
				                                    <Routes>
				                                        <Route path="/" element={ <Babbeln /> } />
				                                        <Route path="/getting-started" element={ <Start /> } />
				                                    </Routes>
			                                        </BrowserRouter>
			                                    }
			                                </ContextmenuContextProvider>
			                            </CallContextProvider>
		                                </ModalContextProvider>
		                            </BlogContextProvider>
		                        </PostContextProvider>
		                    </FriendrequestsContextProvider>
		                </FriendsContextProvider>
	                    </MessageContextProvider>
	                </UserContextProvider>
                    </SocketContextProvider>
	        </NotificationContextProvider>
            </AppContextProvider>
        </main>
    );
};

export default App;
